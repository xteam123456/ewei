<?php
/**
 * 
 *
 */
require '../bootstrap.php';
function getRandomString($len, $chars=null)
{
    if (is_null($chars)){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    }
    mt_srand(10000000*(double)microtime());
    for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++){
        $str .= $chars[mt_rand(0, $lc)];
    }
    return $str;
}

if ($_GPC['__input']) {   
  
  $client_id=$_GPC['__input']['client_id'];
  $client_secret=$_GPC['__input']['client_secret'];
  $grant_type=$_GPC['__input']['grant_type'];
   
  if($grant_type!='client_credential')
  {
      echo json_encode(array( "errcode"=>5001,"errmsg"=>"grant_type错误"));exit;
  }
  else
  {
      
      $sql="select id,uniacid from ".  tablename('ewei_shop_appauth') . " where client_id=:client_id and client_secret=:client_secret ";
      $parm=array(
          ':client_id'=>$client_id,
          ':client_secret'=>$client_secret
      );
      $pp=pdo_fetch($sql,$parm);
      $id=$pp['id'];
      $uniacid=$pp['uniacid'];
      if($id>0)
      {
          $tk['salt']=getRandomString(8);
          $tk['token']=$tk['salt'].sha1(md5($client_id.$client_secret.$tk['salt'].time()));
          
          $tsql="select count(*) from ".  tablename('ewei_shop_appauth') . " where  token=:token" ;  //验证token,一个uniacid对应一个
          $ccp=array(":token"=>$tk['token']);
          $check=pdo_fetchcolumn($tsql,$ccp);
          while($check>0)
          {
              $tk['token']=$tk['salt'].sha1(md5($client_id.$client_secret.$tk['salt'].time()));
              $tsql="select count(*) from ".  tablename('ewei_shop_appauth') . " where  token=:token" ;  
              $ccp=array(":token"=>$tk['token']);
              $check=pdo_fetchcolumn($tsql,$ccp);
          }
          
          $tk['createtime']=time();
          $tk['refreshtime']=intval(time()+2*7*60*60*24); 
          if(pdo_update("ewei_shop_appauth",$tk,array('id'=>$id))>0)
          {
              $ret=array("errcode"=>0,
	                     "errmsg"=>"success",
                         "uniacid"=>$uniacid,              
                         "token"=>$tk['token'],
                         "expires_in"=>604800); 
              echo json_encode($ret);exit;
          }
          else
          {
              echo json_encode(array( "errcode"=>5003,"errmsg"=>"服务器异常"));exit;
          }
      }
      else
      {
          echo json_encode(array( "errcode"=>5002,"errmsg"=>"验证失败"));exit;
      }
  }
}
?>