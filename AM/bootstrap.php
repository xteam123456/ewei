<?php
require '../../framework/bootstrap.inc.php';
if ($_W['setting']['copyright']['status'] == 1) {
    $mess["errmsg"]='抱歉，站点已关闭，关闭原因：' . $_W['setting']['copyright']['reason'];
    $mess["errcode"]="8000";
    echo json_encode($mess);exit;
}

if (empty($_GPC['__input'])&&empty($_SERVER["QUERY_STRING"])) {
    
    $mess["errmsg"]='未接收到数据';
    $mess["errcode"]="8001";
    echo json_encode($mess);exit;
}
function formatBizQueryParaMap($paraMap, $urlencode)
{
    $buff = "";
    ksort($paraMap);
    foreach ($paraMap as $k => $v)
    {
        if($urlencode)
        {
            $v = urlencode($v);
        }
        //$buff .= strtolower($k) . "=" . $v . "&";
        $buff .= $k . "=" . $v . "&";
    }
    if (strlen($buff) > 0)
    {
        $reqPar = substr($buff, 0, strlen($buff)-1);
    }
    return $reqPar;
}
function getSign($Obj,$timestamp)
{
    foreach ($Obj as $k => $v)
    {
        $Parameters[$k] = $v;
    }
    //签名步骤一：按字典序排序参数
    ksort($Parameters);
    $String = formatBizQueryParaMap($Parameters, false);
    // echo '【string1】'.$String.'</br>';
    //签名步骤二：MD5加密
    $String=$timestamp.'&'.MD5($String);
    $String = MD5($String);
    //echo "【string3】 ".$String."</br>";
    //签名步骤三：所有字符转为大写
    $result_ = strtoupper($String);
    //echo "【result】 ".$result_."</br>";
    return $result_;
}
function checkparm($post,$AM)
{
    if(empty($post['timestamp']))
    {
        echo json_encode(array( "errcode"=>1002,"errmsg"=>"timestamp不能为空"));exit;
    }
    if(empty($post['sign']))
    {
        echo json_encode(array( "errcode"=>1003,"errmsg"=>"sign不能为空"));exit;
    }
    
    $sign=$post['sign'];
    $timestamp=$post['timestamp'];
    unset($post['sign']);
    unset($post['timestamp']);
    unset($post['token']);
    unset($post['resource']);
    unset($post['sls']);
    if(!empty($AM['table']))$post['table']=rawurlencode($AM['table']); 
    if(!empty($AM['ids']))$post['ids']=rawurlencode($AM['ids']);   //插入
    if(!empty($AM['id']))$post['id']=rawurlencode($AM['id']);    //删除
    if(!empty($AM['filter']))$post['filter']=rawurlencode($AM['filter']);  
    $checksign=getSign($post,$timestamp);
    if($checksign!=$sign)
    {
        echo json_encode(array( "errcode"=>1004,"errmsg"=>"sign错误"));exit;
    }
}

$post = $_GPC['__input'];      //post进来的数据
if(!empty($_SERVER["QUERY_STRING"]))   //get进来的数据
{
    $get=explode('&', $_SERVER["QUERY_STRING"]);
    $post['token']=explode('=', $get[0])[1];

}
if(!(strpos($_SERVER['PHP_SELF'],"token.php")>0))
{
    $token=$post['token'];
    $client_id=$post['client_id'];
    if(empty($post['token']))
    {
        echo json_encode(array( "errcode"=>1001,"errmsg"=>"token不能为空"));exit;
    }

    $tsql="select uniacid,createtime,client_id from ".  tablename('ewei_shop_appauth') . " where  token=:token" ;  //验证token，唯一的
    $ccp=array(":token"=>$token);
    $auth=pdo_fetch($tsql,$ccp);

    $_W['uniacid']=$auth['uniacid'];
    $post['client_id']=$auth['client_id'];    //用作自己签名
    if(empty($_W['uniacid'])||intval($auth['createtime']+604800)<time())
    {
         echo json_encode(array( "errcode"=>1006,"errmsg"=>"token无效"));exit;
    }
}
?>
