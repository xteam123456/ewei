<?php
require '../bootstrap.php';
checkparm($post);

$username = trim($post['username']);
$password = trim($post['password']);
$repassword = trim($post['repassword']);

if($repassword != $password ){
    echo json_encode(array( "errcode"=>6001,"errmsg"=>"两次密码不一致"));exit;
}

$userinfo = array(
    'openid' => $username,
    'nickname' => '',
    'subscribe' => '0',
    'subscribe_time' => '',
);
$record = array(
    'openid' => $userinfo['openid'],
    'uid' => 0,
    'acid' => 0,
    'uniacid' => $_W['uniacid'],
    'salt' => random(8),
    'updatetime' => TIMESTAMP,
    'nickname' => stripslashes($userinfo['nickname']),
    'follow' => $userinfo['subscribe'],
    'followtime' => $userinfo['subscribe_time'],
    'unfollowtime' => 0,
    'tag' => base64_encode(iserializer($userinfo))
);
$member = array(
    'uniacid' => $_W['uniacid'],
    'uid' => 0,
    'openid' => $username,
    'realname' => $username,
    'mobile' => $username,
    'nickname' => stripslashes($userinfo['nickname']),
    'createtime' => time(),
    'status' => 0,
	'isapp'=>1
);


$sql = 'SELECT `uid` FROM ' . tablename('mc_members') . ' WHERE `uniacid`=:uniacid AND `mobile`=:mobile and apptype=1';
$pars[':uniacid'] = $_W['uniacid'];
if(preg_match(REGULAR_MOBILE, $username)) {
    $pars[':mobile'] = $username;
}
else{ echo json_encode(array( "errcode"=>6002,"errmsg"=>"您输入的用户名格式错误,请输入正确的手机号"));exit;}
$user = pdo_fetchcolumn($sql, $pars);
if(!empty($user)) {
        echo json_encode(array( "errcode"=>6003,"errmsg"=>"该手机号已经注册"));exit;
}
$default_groupid = pdo_fetchcolumn('SELECT groupid FROM ' .tablename('mc_groups') . ' WHERE uniacid = :uniacid AND isdefault = 1', array(':uniacid' => $_W['uniacid']));
$data = array(
        'uniacid' => $_W['uniacid'],
        'salt' => random(8),
        'groupid' => $default_groupid,
        'createtime' => TIMESTAMP,
);     
$data['mobile'] = $username;
$data['password'] = md5($password . $data['salt'] . $_W['config']['setting']['authkey']);   //加盐MD5  
$data['nickname'] = $record['nickname']; 
$data['apptype']=1;
if(pdo_insert('mc_members', $data)>0)
{
    $record['uid'] = pdo_insertid();
    pdo_insert('mc_mapping_fans', $record);
    $member['uid']= $record['uid'];
    pdo_insert('ewei_shop_member', $member);
    echo json_encode(array( "errcode"=>0,"errmsg"=>"注册成功"));exit;
}
else
{
    echo json_encode(array( "errcode"=>6004,"errmsg"=>"注册失败"));exit;
}
?>