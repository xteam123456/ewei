<?php
/**
 * [Weizan System] Copyright (c) 2014 012WZ.COM
 * Weizan is NOT a free software, it under the license terms, visited http://gxxsh.com.cn/ for more details.
 */
require './framework/bootstrap.inc.php';
//litest222asd
$host = $_SERVER['HTTP_HOST'];
if (!empty($host)) {
	$bindhost = pdo_fetch("SELECT * FROM ".tablename('site_multi')." WHERE bindhost = :bindhost", array(':bindhost' => $host));
	if (!empty($bindhost)) {
		header("Location: ". $_W['siteroot'] . 'app/index.php?i='.$bindhost['uniacid'].'&t='.$bindhost['id']);
		exit;
	}
}
if($_W['os'] == 'mobile' && (!empty($_GPC['i']) || !empty($_SERVER['QUERY_STRING']))) {
	header('Location: ./app/index.php?' . $_SERVER['QUERY_STRING']);
} else {
	if(empty($_SERVER['QUERY_STRING'])||$_SERVER['QUERY_STRING']=="refresh")$_SERVER['QUERY_STRING']="c=user&a=login&";
	header('Location: ./web/index.php?' . $_SERVER['QUERY_STRING']);
}