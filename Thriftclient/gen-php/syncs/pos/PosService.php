<?php
namespace syncs\pos;
/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TBinaryProtocolAccelerated;
use Thrift\Exception\TApplicationException;


interface PosServiceIf {
  /**
   * @param string $storeCode
   * @param string $name
   * @param string $randomCode
   * @return \syncs\pos\Pos
   * @throws \syncs\pos\StoreNotFound
   */
  public function postPos($storeCode, $name, $randomCode);
  /**
   * @param \syncs\shared\PosContext $posContext
   * @return bool
   * @throws \syncs\exceptions\PosNotAuthorized
   */
  public function deletePos(\syncs\shared\PosContext $posContext);
}

class PosServiceClient implements \syncs\pos\PosServiceIf {
  protected $input_ = null;
  protected $output_ = null;

  protected $seqid_ = 0;

  public function __construct($input, $output=null) {
    $this->input_ = $input;
    $this->output_ = $output ? $output : $input;
  }

  public function postPos($storeCode, $name, $randomCode)
  {
    $this->send_postPos($storeCode, $name, $randomCode);
    return $this->recv_postPos();
  }

  public function send_postPos($storeCode, $name, $randomCode)
  {
    $args = new \syncs\pos\PosService_postPos_args();
    $args->storeCode = $storeCode;
    $args->name = $name;
    $args->randomCode = $randomCode;
    $bin_accel = ($this->output_ instanceof TBinaryProtocolAccelerated) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'postPos', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('postPos', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_postPos()
  {
    $bin_accel = ($this->input_ instanceof TBinaryProtocolAccelerated) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, '\syncs\pos\PosService_postPos_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new \syncs\pos\PosService_postPos_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    if ($result->storenf !== null) {
      throw $result->storenf;
    }
    throw new \Exception("postPos failed: unknown result");
  }

  public function deletePos(\syncs\shared\PosContext $posContext)
  {
    $this->send_deletePos($posContext);
    return $this->recv_deletePos();
  }

  public function send_deletePos(\syncs\shared\PosContext $posContext)
  {
    $args = new \syncs\pos\PosService_deletePos_args();
    $args->posContext = $posContext;
    $bin_accel = ($this->output_ instanceof TBinaryProtocolAccelerated) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'deletePos', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('deletePos', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_deletePos()
  {
    $bin_accel = ($this->input_ instanceof TBinaryProtocolAccelerated) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, '\syncs\pos\PosService_deletePos_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new \syncs\pos\PosService_deletePos_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    if ($result->posna !== null) {
      throw $result->posna;
    }
    throw new \Exception("deletePos failed: unknown result");
  }

}

// HELPER FUNCTIONS AND STRUCTURES

class PosService_postPos_args {
  static $_TSPEC;

  /**
   * @var string
   */
  public $storeCode = null;
  /**
   * @var string
   */
  public $name = null;
  /**
   * @var string
   */
  public $randomCode = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'storeCode',
          'type' => TType::STRING,
          ),
        2 => array(
          'var' => 'name',
          'type' => TType::STRING,
          ),
        3 => array(
          'var' => 'randomCode',
          'type' => TType::STRING,
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['storeCode'])) {
        $this->storeCode = $vals['storeCode'];
      }
      if (isset($vals['name'])) {
        $this->name = $vals['name'];
      }
      if (isset($vals['randomCode'])) {
        $this->randomCode = $vals['randomCode'];
      }
    }
  }

  public function getName() {
    return 'PosService_postPos_args';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->storeCode);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 2:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->name);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 3:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->randomCode);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('PosService_postPos_args');
    if ($this->storeCode !== null) {
      $xfer += $output->writeFieldBegin('storeCode', TType::STRING, 1);
      $xfer += $output->writeString($this->storeCode);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->name !== null) {
      $xfer += $output->writeFieldBegin('name', TType::STRING, 2);
      $xfer += $output->writeString($this->name);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->randomCode !== null) {
      $xfer += $output->writeFieldBegin('randomCode', TType::STRING, 3);
      $xfer += $output->writeString($this->randomCode);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

class PosService_postPos_result {
  static $_TSPEC;

  /**
   * @var \syncs\pos\Pos
   */
  public $success = null;
  /**
   * @var \syncs\pos\StoreNotFound
   */
  public $storenf = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::STRUCT,
          'class' => '\syncs\pos\Pos',
          ),
        1 => array(
          'var' => 'storenf',
          'type' => TType::STRUCT,
          'class' => '\syncs\pos\StoreNotFound',
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['success'])) {
        $this->success = $vals['success'];
      }
      if (isset($vals['storenf'])) {
        $this->storenf = $vals['storenf'];
      }
    }
  }

  public function getName() {
    return 'PosService_postPos_result';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 0:
          if ($ftype == TType::STRUCT) {
            $this->success = new \syncs\pos\Pos();
            $xfer += $this->success->read($input);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 1:
          if ($ftype == TType::STRUCT) {
            $this->storenf = new \syncs\pos\StoreNotFound();
            $xfer += $this->storenf->read($input);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('PosService_postPos_result');
    if ($this->success !== null) {
      if (!is_object($this->success)) {
        throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
      }
      $xfer += $output->writeFieldBegin('success', TType::STRUCT, 0);
      $xfer += $this->success->write($output);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->storenf !== null) {
      $xfer += $output->writeFieldBegin('storenf', TType::STRUCT, 1);
      $xfer += $this->storenf->write($output);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

class PosService_deletePos_args {
  static $_TSPEC;

  /**
   * @var \syncs\shared\PosContext
   */
  public $posContext = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'posContext',
          'type' => TType::STRUCT,
          'class' => '\syncs\shared\PosContext',
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['posContext'])) {
        $this->posContext = $vals['posContext'];
      }
    }
  }

  public function getName() {
    return 'PosService_deletePos_args';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::STRUCT) {
            $this->posContext = new \syncs\shared\PosContext();
            $xfer += $this->posContext->read($input);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('PosService_deletePos_args');
    if ($this->posContext !== null) {
      if (!is_object($this->posContext)) {
        throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
      }
      $xfer += $output->writeFieldBegin('posContext', TType::STRUCT, 1);
      $xfer += $this->posContext->write($output);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

class PosService_deletePos_result {
  static $_TSPEC;

  /**
   * @var bool
   */
  public $success = null;
  /**
   * @var \syncs\exceptions\PosNotAuthorized
   */
  public $posna = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::BOOL,
          ),
        1 => array(
          'var' => 'posna',
          'type' => TType::STRUCT,
          'class' => '\syncs\exceptions\PosNotAuthorized',
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['success'])) {
        $this->success = $vals['success'];
      }
      if (isset($vals['posna'])) {
        $this->posna = $vals['posna'];
      }
    }
  }

  public function getName() {
    return 'PosService_deletePos_result';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 0:
          if ($ftype == TType::BOOL) {
            $xfer += $input->readBool($this->success);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 1:
          if ($ftype == TType::STRUCT) {
            $this->posna = new \syncs\exceptions\PosNotAuthorized();
            $xfer += $this->posna->read($input);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('PosService_deletePos_result');
    if ($this->success !== null) {
      $xfer += $output->writeFieldBegin('success', TType::BOOL, 0);
      $xfer += $output->writeBool($this->success);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->posna !== null) {
      $xfer += $output->writeFieldBegin('posna', TType::STRUCT, 1);
      $xfer += $this->posna->write($output);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}


