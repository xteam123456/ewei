<?php
global $_W, $_GPC;
$goods   = array();
$openid  = trim($_GPC['openid']);

//$content = trim(urldecode($_GPC['content']));
$content = "代理证书";
///$authbook  = empty($_GPC['authbook'])?'':$_GPC['authbook'];
$gid=$_GPC['gid'];
if (empty($openid)) {
	 show_json(1, array(
				"status"=>1,
	            "msg"=>"不存在该用户"
        ));
   // exit;
}
$member = m('member')->getMember($openid);


if (!empty($member['level'])) {
        $level = m('member')->getLevel($openid);
}
else
{
    $level['levelname'] =  '普通会员';

}
$member['level']=empty($member['level'])?'-1':$member['level'];
if (empty($member)) {
	 show_json(1, array(
				"status"=>2,
	            "msg"=>"不存在该用户"
        ));
   // exit("用户不存在");
}
if (strexists($content, '+')) {
    $msg    = explode('+', $content);
    $poster = pdo_fetch('select * from ' . tablename('ewei_shop_poster') . ' where keyword=:keyword and type=3 and isdefault=1 and uniacid=:uniacid and plevel=:level limit 1', array(
        ':keyword' => $msg[0],
        ':uniacid' => $_W['uniacid'],
		':level'   => $member['level']
    ));
    if (empty($poster)) {
        m('message')->sendCustomNotice($openid, '未找到商品海报类型!');
        exit;
    }
    $goodsid = intval($msg[1]);
    if (empty($goodsid)) {
        m('message')->sendCustomNotice($openid, '未找到商品, 无法生成海报 !');
        exit;
    }
} else {
	
    $poster = pdo_fetch('select * from ' . tablename('ewei_shop_poster') . ' where keyword=:keyword and isdefault=1 and uniacid=:uniacid and plevel=:level limit 1', array(
        ':keyword' => $content,
        ':uniacid' => $_W['uniacid'],
		':level'   => $member['level']
    ));
    if (empty($poster)) {
       // m('message')->sendCustomNotice($openid, '未找到海报类型!');
        show_json(1, array(
            "status"=>3,
            "msg"=>"不存在改证书"
        ));
        exit;
    }
}
/*if ($member['isagent'] != 1 || $member['status'] != 1) {
    if (empty($poster['isopen'])) {
        $opentext = !empty($poster['opentext']) ? $poster['opentext'] : '您还不是我们分销商，去努力成为分销商，拥有你的专属海报吧!';
        m('message')->sendCustomNotice($openid, $opentext, trim($poster['openurl']));
        exit;
    }
}
if (!empty($poster['waittext'])) {
    m('message')->sendCustomNotice($openid, $poster['waittext']);
}*/
$qr = $this->model->getQR($poster, $member, $goodsid);
if (is_error($qr)) {
   // m('message')->sendCustomNotice($openid, '生成二维码出错: ' . $qr['message']);
    //exit("二维码生成失败");
	show_json(1, array(
				"status"=>4,
	           "msg"=>"二维码生成出错"
        ));
}
if(empty($gid))
{
    show_json(1, array(
        "status"=>5,
        "msg"=>"未找到商品"
    ));
}
$auth = pdo_fetch('select title,starttime,endtime,authorid from ' . tablename('ewei_shop_authgoods') . ' where uniacid=:uniacid and goodsid=:goodsid and openid=:openid', array(
    ':uniacid' => $_W['uniacid'],
    ':goodsid' =>$gid,
    ':openid'  =>$openid
));
$member['auth']=$auth;
$member['auth']['level']=$level['levelname'];
$member['nidcard']=substr($member['idcard'],0,6)."********".substr($member['idcard'],-4);
$img     = $this->model->createPoster($poster, $member, $qr);

include $this->template('member/authimg');
/*$mediaid = $img['mediaid'];
if (!empty($mediaid)) {
    if (!empty($poster['oktext'])) {
        m('message')->sendCustomNotice($openid, $poster['oktext']);
    }
    m('message')->sendImage($openid, $mediaid);
} else {
    $oktext .= "\n<a href='" . $img['img'] . "'>点击查看您的专属海报</a>";
    m('message')->sendCustomNotice($openid, $oktext);
}
exit;*/