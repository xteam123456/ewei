<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
class Ewei_DShop_Common
{
	function stock($orderid)
    {
        $ordergoods = pdo_fetch('select total,goodsid,optionid from ' . tablename('ewei_shop_order_goods'). " where orderid={$orderid}");
        if(empty($ordergoods['optionid']))
        {
            $opid  = pdo_fetchcolumn('SELECT parentid FROM ' . tablename('ewei_shop_goods') ." where id={$ordergoods['goodsid']} ");
            if(!empty($opid)){
                $ostock  = pdo_fetchcolumn('SELECT total FROM ' . tablename('ewei_shop_goods') ." where id={$opid} limit 1");
                $newstock=  intval($ostock- $ordergoods['total']);
                pdo_update('ewei_shop_goods', array('total'=>$newstock), array('id' => $opid));
                pdo_update('ewei_shop_goods', array('total'=>$newstock), array('parentid' => $opid));
            }
        }
        else
        {
            $opid  = pdo_fetchcolumn('SELECT parentid FROM ' . tablename('ewei_shop_goods_option') ." where id={$ordergoods['optionid']} ");
            if(!empty($opid)){
                $ostock  = pdo_fetchcolumn('SELECT stock FROM ' . tablename('ewei_shop_goods_option') ." where id={$opid} limit 1");
                $newstock=  intval($ostock- $ordergoods['total']);
                pdo_update('ewei_shop_goods_option', array('stock'=>$newstock), array('id' => $opid));
                pdo_update('ewei_shop_goods_option', array('stock'=>$newstock), array('parentid' => $opid));
            }
        }
    }
	public function xhcharge($data)
	{
		$MOBILE=$data['MOBILE'];
		$PRODUCTCODE=$data['PRODUCTCODE'];		
		$url="http://api.liuliangshop.com/v1/auth/token";
		$dd['client_id']='flow6754a62ff2qatftmclzl';
		$dd['client_secret']='emchkiyi87vtwmv3bl53ltjlb47mqt1449050592';
		$dd['grant_type']='client_credential';
		$file='xh.txt';
		if(file_exists($file)&& (mktime() - filemtime($file) < 7000))
		{
			$token=file_get_contents($file);
		}
		else
		{
			$result=json_decode($this->curlPost($url,$dd));
			if($result->errcode===0){
				$token=$result->access_token;
				file_put_contents($file,$token);
			}
			else
			{
				$re['STATUS']=8000;
				$re['MESSAGE']="token为空"; //空token
			}
		}
		if(!empty($token))
		{
			$churl="http://api.liuliangshop.com/v1/flow/recharge/order?access_token=$token";
			$sitepath = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], '/'));
			$siteurl=htmlspecialchars('http://' . $_SERVER['HTTP_HOST'] . $sitepath);
			$callback_url=$siteurl.'/xhcallback.php';
			$sign = $this->getSign(
					array(
							'callback_url' => $callback_url,
							'client_id' => $dd['client_id'],
							'number' => $MOBILE,
							'product_id' =>$PRODUCTCODE
						)
				);
			$orinfo['sign']=$sign;
			$orinfo['number']=$MOBILE;
			$orinfo['product_id']=$PRODUCTCODE;
			$orinfo['callback_url']=$callback_url;
			$orderinfo=json_decode($this->curlPost($churl,$orinfo));
			if(empty($orderinfo))
			{
				$re['STATUS']=8001;
				$re['MESSAGE']="接口错误";
			}
			else{
				$re['STATUS']=$orderinfo->errcode;
				$re['MESSAGE']=$orderinfo->errmsg;
			}
		}
		else
		{
			$re['STATUS']=8000;
			$re['MESSAGE']="token为空"; //空token
		}
		return $re;
		
	}
	public function xzcharge($data)
	{
		$MOBILE=$data['MOBILE'];
		$PRODUCTCODE=$data['PRODUCTCODE'];
		$CUSTOMORDERNUM=$data['CUSTOMORDERNUM'];
		$UserName = "c5a719683348984bfd1247ae4a815a17";
		$Password = "3fd3dd09450d41cb9363f86d1c2e453c";
		$ApiKey = "e35bb214ba76dcf610aea7c980a99a94";
		$ApiUrl = "http://www.xz520.cn/interface/submitOrder.json?";
		$signSource="MOBILE=".$MOBILE ."&PASSWORD=".$Password."&PRODUCTCODE=".$PRODUCTCODE."&USERNAME=".$UserName;
		$sign=strtolower(md5($signSource.$ApiKey));
		$str=$signSource."&SIGN=".$sign."&CUSTOMORDERNUM=".$CUSTOMORDERNUM;
		$url=$ApiUrl.$str;
		$result=json_decode($this->curlGet($url));
		if(empty($result))
		{
			$re['STATUS']=8001;
			$re['MESSAGE']="接口错误";
		}
		else{
			$re['STATUS']=$result->STATUS;
			$re['MESSAGE']=$result->MESSAGE;	
		}		
		return $re;
	}
	public function chargell($data)
	{
		$send=1;
		$remark=$phone = str_replace(' ','',$data['remark']);
		$gdata= pdo_fetchall("select productsn from " . tablename('ewei_shop_order_goods') . ' where orderid=:orderid ', array(
            ':orderid' => $data['id']
        ));
		if(empty($phone)){
		$phone = pdo_fetchcolumn('select mobile from ' . tablename('ewei_shop_member_address') . ' where id=:id and openid=:openid and uniacid=:uniacid   limit 1', array(
                ':uniacid' => $data['uniacid'],
                ':openid' => $data['openid'],
                ':id' => $data['addressid']
            ));	
		}
		foreach ($gdata as $rows)
		{	
				if(strpos($rows['productsn'],':')!==false)
				{
				$row=explode(":", $rows['productsn']);
				$lldata['MOBILE']=$phone;
				$lldata['PRODUCTCODE']=$row[1];
				$lldata['CUSTOMORDERNUM']=$data['ordersn'];
				if($row[0]=="xz")$result=$this->xzcharge($lldata);
				if($row[0]=="xh")$result=$this->xhcharge($lldata);
				if($result['STATUS']===0)$send=2;
				$remark=$remark."(流量[".$rows['productsn']."]".$result["MESSAGE"].":".$result['STATUS'].")";
				}			
		}
		pdo_update('ewei_shop_order', array(
				'sendtime'=>time(),
				'remark' => $remark,
				'status' => $send
		), array(
				'id' => $data['id']
		));
		if($send==2)
		{
			m('notice')->sendOrderMessage($data['id']);
			plog('order.op.send', "订单发货 ID: {$data['id']} 订单号: {$data['ordersn']} ");
		}	
	    
	}
    public function getSetData($uniacid = 0)
    {
        global $_W;
        if (empty($uniacid)) {
            $uniacid = $_W['uniacid'];
        }
        $set = m('cache')->getArray('sysset', $uniacid);
        if (empty($set)) {
            $set = pdo_fetch("select * from " . tablename('ewei_shop_sysset') . ' where uniacid=:uniacid limit 1', array(
                ':uniacid' => $uniacid
            ));
            if (empty($set)) {
                $set = array();
            }
            m('cache')->set('sysset', $set, $uniacid);
        }
        return $set;
    }
    public function getSysset($key = '', $uniacid = 0)
    {
        global $_W, $_GPC;
        $set     = $this->getSetData($uniacid);
        $allset  = unserialize($set['sets']);
        $retsets = array();
        if (!empty($key)) {
            if (is_array($key)) {
                foreach ($key as $k) {
                    $retsets[$k] = isset($allset[$k]) ? $allset[$k] : array();
                }
            } else {
                $retsets = isset($allset[$key]) ? $allset[$key] : array();
            }
            return $retsets;
        } else {
            return $allset;
        }
    }
    public function alipay_build($params, $alipay = array(), $type = 0, $openid = '')
    {
        global $_W;
        $tid                   = $params['tid'];
        $set                   = array();
        $set['service']        = 'alipay.wap.create.direct.pay.by.user';
        $set['partner']        = $alipay['partner'];
        $set['_input_charset'] = 'utf-8';
        $set['sign_type']      = 'MD5';
        if (empty($type)) {
            $set['notify_url'] = $_W['siteroot'] . "addons/ewei_shop/payment/alipay/notify.php";
            $set['return_url'] = $_W['siteroot'] . "app/index.php?i={$_W['uniacid']}&c=entry&m=ewei_shop&do=order&p=pay&op=return&openid=" . $openid;
        } else {
            $set['notify_url'] = $_W['siteroot'] . "addons/ewei_shop/payment/alipay/notify.php";
            $set['return_url'] = $_W['siteroot'] . "app/index.php?i={$_W['uniacid']}&c=entry&m=ewei_shop&do=member&p=recharge&op=return&openid=" . $openid;
        }
        $set['out_trade_no'] = $tid;
        $set['subject']      = $params['title'];
        $set['total_fee']    = $params['fee'];
        $set['seller_id']    = $alipay['account'];
        $set['payment_type'] = 1;
        $set['body']         = $_W['uniacid'] . ':' . $type;
        $prepares            = array();
        foreach ($set as $key => $value) {
            if ($key != 'sign' && $key != 'sign_type') {
                $prepares[] = "{$key}={$value}";
            }
        }
        sort($prepares);
        $string = implode($prepares, '&');
        $string .= $alipay['secret'];
        $set['sign'] = md5($string);
        return array(
            'url' => ALIPAY_GATEWAY . '?' . http_build_query($set, '', '&')
        );
    }
    function wechat_build($params, $wechat, $type = 0)
    {
        global $_W;
        load()->func('communication');
        if (empty($wechat['version']) && !empty($wechat['signkey'])) {
            $wechat['version'] = 1;
        }
        $wOpt = array();
        if ($wechat['version'] == 1) {
            $wOpt['appId']               = $wechat['appid'];
            $wOpt['timeStamp']           = TIMESTAMP . "";
            $wOpt['nonceStr']            = random(8) . "";
            $package                     = array();
            $package['bank_type']        = 'WX';
            $package['body']             = urlencode($params['title']);
            $package['attach']           = $_W['uniacid'] . ':' . $type;   //公众号id+type
            $package['partner']          = $wechat['partner'];
            $package['device_info']      = "ewei_shop";
            $package['out_trade_no']     = $params['tid'];
            $package['total_fee']        = $params['fee'] * 100;
            $package['fee_type']         = '1';
            $package['notify_url']       = $_W['siteroot'] . "addons/ewei_shop/payment/wechat/notify.php";
            $package['spbill_create_ip'] = CLIENT_IP;
            $package['input_charset']    = 'UTF-8';
            ksort($package);
            $string1 = '';
            foreach ($package as $key => $v) {
                if (empty($v)) {
                    continue;
                }
                $string1 .= "{$key}={$v}&";
            }
            $string1 .= "key={$wechat['key']}";
            $sign    = strtoupper(md5($string1));
            $string2 = '';
            foreach ($package as $key => $v) {
                $v = urlencode($v);
                $string2 .= "{$key}={$v}&";
            }
            $string2 .= "sign={$sign}";
            $wOpt['package'] = $string2;
            $string          = '';
            $keys            = array(
                'appId',
                'timeStamp',
                'nonceStr',
                'package',
                'appKey'
            );
            sort($keys);
            foreach ($keys as $key) {
                $v = $wOpt[$key];
                if ($key == 'appKey') {
                    $v = $wechat['signkey'];
                }
                $key = strtolower($key);
                $string .= "{$key}={$v}&";
            }
            $string           = rtrim($string, '&');
            $wOpt['signType'] = 'SHA1';
            $wOpt['paySign']  = sha1($string);
            return $wOpt;
        } else {
            $package              = array();
            $package['appid']     = $wechat['appid'];
            $package['mch_id']    = $wechat['mchid'];
            $package['nonce_str'] = random(8) . "";
            $package['body']             = $params['title'];
            $package['device_info']      = "ewei_shop";
            $package['attach']           = $_W['uniacid'] . ':' . $type;
            $package['out_trade_no']     = $params['tid'];
            $package['total_fee']        = $params['fee'] * 100;
            $package['spbill_create_ip'] = CLIENT_IP;
            $package['notify_url']       = $_W['siteroot'] . "addons/ewei_shop/payment/wechat/notify.php";
            $package['trade_type']       = 'JSAPI';
            $package['openid']           = $_W['fans']['from_user'];
            ksort($package, SORT_STRING);
            $string1 = '';
            foreach ($package as $key => $v) {
                if (empty($v)) {
                    continue;
                }
                $string1 .= "{$key}={$v}&";
            }
            $string1 .= "key={$wechat['signkey']}";
            $package['sign'] = strtoupper(md5($string1));
            $dat             = array2xml($package);
            $response        = ihttp_request('https://api.mch.weixin.qq.com/pay/unifiedorder', $dat);
            if (is_error($response)) {
                return $response;
            }
            $xml = @simplexml_load_string($response['content'], 'SimpleXMLElement', LIBXML_NOCDATA);
            if (strval($xml->return_code) == 'FAIL') {
                return error(-1, strval($xml->return_msg));
            }
            if (strval($xml->result_code) == 'FAIL') {
                return error(-1, strval($xml->err_code) . ': ' . strval($xml->err_code_des));
            }
            $prepayid          = $xml->prepay_id;
            $wOpt['appId']     = $wechat['appid'];
            $wOpt['timeStamp'] = TIMESTAMP . "";
            $wOpt['nonceStr']  = random(8) . "";
            $wOpt['package']   = 'prepay_id=' . $prepayid;
            $wOpt['signType']  = 'MD5';
            ksort($wOpt, SORT_STRING);
            foreach ($wOpt as $key => $v) {
                $string .= "{$key}={$v}&";
            }
            $string .= "key={$wechat['signkey']}";
            $wOpt['paySign'] = strtoupper(md5($string));
            return $wOpt;
        }
    }
    public function getAccount()
    {
        global $_W;
        load()->model('account');
        if (!empty($_W['acid'])) {
            return WeAccount::create($_W['acid']);
        } else {
            $acid = pdo_fetchcolumn("SELECT acid FROM " . tablename('account_wechats') . " WHERE `uniacid`=:uniacid LIMIT 1", array(
                ':uniacid' => $_W['uniacid']
            ));
            return WeAccount::create($acid);
        }
        return false;
    }
    public function shareAddress()
    {
        global $_W, $_GPC;
        $appid  = $_W['account']['key'];
        $secret = $_W['account']['secret'];
        load()->func('communication');
        $url = $_W['siteroot'] . "app/index.php?" . $_SERVER['QUERY_STRING'];
        if (empty($_GPC['code'])) {
            $oauth2_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" . $appid . "&redirect_uri=" . urlencode($url) . "&response_type=code&scope=snsapi_base&state=123#wechat_redirect";
            header("location: $oauth2_url");
            exit();
        }
        $code      = $_GPC['code'];
        $token_url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" . $appid . "&secret=" . $secret . "&code=" . $code . "&grant_type=authorization_code";
        $resp      = ihttp_get($token_url);
        $token     = @json_decode($resp['content'], true);
        if (empty($token) || !is_array($token) || empty($token['access_token']) || empty($token['openid'])) {
            return false;
        }
        $package = array(
            "appid" => $appid,
            "url" => $url,
            'timestamp' => time() . "",
            'noncestr' => random(8, true) . "",
            'accesstoken' => $token['access_token']
        );
        ksort($package, SORT_STRING);
        $addrSigns = array();
        foreach ($package as $k => $v) {
            $addrSigns[] = "{$k}={$v}";
        }
        $string   = implode('&', $addrSigns);
        $addrSign = strtolower(sha1(trim($string)));
        $data     = array(
            "appId" => $appid,
            "scope" => "jsapi_address",
            "signType" => "sha1",
            "addrSign" => $addrSign,
            "timeStamp" => $package['timestamp'],
            "nonceStr" => $package['noncestr']
        );
        return $data;
    }
    public function createNO($table, $field, $prefix,$long=6)
    {
    	$billno = date('YmdHis') . random($long, true);
        while (1) {
            $count = pdo_fetchcolumn('select count(*) from ' . tablename('ewei_shop_' . $table) . " where {$field}=:billno limit 1", array(
                ':billno' => $billno
            ));
            if ($count <= 0) {
                break;
            }
            $billno = date('YmdHis') . random($long, true);
           
        }
        return $prefix . $billno;
    }
    public function html_images($detail = '')
    {
        $detail = htmlspecialchars_decode($detail);
        preg_match_all("/<img.*?src=[\'| \"](.*?(?:[\.gif|\.jpg|\.png|\.jpeg]?))[\'|\"].*?[\/]?>/", $detail, $imgs);
        $images = array();
        if (isset($imgs[1])) {
            foreach ($imgs[1] as $img) {
                $im       = array(
                    "old" => $img,
                    "new" => save_media($img)
                );
                $images[] = $im;
            }
        }
        foreach ($images as $img) {
            $detail = str_replace($img['old'], $img['new'], $detail);
        }
        return $detail;
    }
    public function getSec($uniacid = 0)
    {
        global $_W;
        if (empty($uniacid)) {
            $uniacid = $_W['uniacid'];
        }
        $set = pdo_fetch("select sec from " . tablename('ewei_shop_sysset') . ' where uniacid=:uniacid limit 1', array(
            ':uniacid' => $uniacid
        ));
        if (empty($set)) {
            $set = array();
        }
        return $set;
    }
    public function paylog($log = '')
    {
        global $_W;
        $openpaylog = m('cache')->getString('paylog', 'global');
        if (!empty($openpaylog)) {
            $path = IA_ROOT . "/addons/ewei_shop/data/paylog/" . $_W['uniacid'] . "/" . date('Ymd');
            if (!is_dir($path)) {
                load()->func('file');
                @mkdirs($path, '0777');
            }
            $file = $path . "/" . date('H') . '.log';
            file_put_contents($file, $log, FILE_APPEND);
        }
    }
    public function curlGet($url){
    	$ch = curl_init();
    	$header = "Accept-Charset: utf-8";
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    	curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$temp = curl_exec($ch);
    	curl_close($ch);
    	return $temp;
    }
    public function curlPost($url, $data){
    	$ch = curl_init();
    	$header = "Accept-Charset: utf-8";
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    	curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$result=curl_exec($ch);
    	return $result;
    }
    public function formatBizQueryParaMap($paraMap, $urlencode)
    {
    	$buff = "";
    	ksort($paraMap);
    	foreach ($paraMap as $k => $v)
    	{
    		if($urlencode)
    		{
    			$v = urlencode($v);
    		}
    		//$buff .= strtolower($k) . "=" . $v . "&";
    		$buff .= $k . "=" . $v . "&";
    	}
    	$reqPar;
    	if (strlen($buff) > 0)
    	{
    		$reqPar = substr($buff, 0, strlen($buff)-1);
    	}
    	return $reqPar;
    }
    protected function getSign($Obj)
    {
    	foreach ($Obj as $k => $v)
    	{
    		$Parameters[$k] = $v;
    	}
    	//签名步骤一：按字典序排序参数
    	ksort($Parameters);
    	$String = $this->formatBizQueryParaMap($Parameters, false);
    	//echo '【string1】'.$String.'</br>';
    	//签名步骤二：sha1加密
    	$String = sha1($String);
    	//echo "【string3】 ".$String."</br>";
    	//签名步骤三：所有字符转为大写
    	$result_ = strtoupper($String);
    	//echo "【result】 ".$result_."</br>";
    	return $result_;
    }
}