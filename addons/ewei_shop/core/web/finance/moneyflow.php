<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;

$op      = $operation = $_GPC['op'] ? $_GPC['op'] : 'display';
$uniacid = $_W['uniacid'];
$puniacid= $_W['uniacid'];
if ($op == 'display') {
    if(empty($_GPC['orderby'])){
        $orderby=" order by tb1.addtime desc";
    }
    else
    {
        $orderby=" order by ".$_GPC['orderby'].$_GPC['torderby'];
    }
    $pindex = max(1, intval($_GPC['page']));
    $psize  = 20;
    if(!empty($_GPC['puniacid']))$puniacid=$_GPC['puniacid'];  
    $condition1 = " and supuniacid={$puniacid} ";
    $condition2 = " and saluniacid={$puniacid} ";   
    if (!empty($_GPC['ordersn'])) {
        $_GPC['ordersn'] = trim($_GPC['ordersn']);
        $condition .= ' and ordersn like :ordersn';
        $params[':ordersn'] = "%{$_GPC['ordersn']}%";
    }
    if (empty($starttime) || empty($endtime)) {
        $starttime = strtotime('-1 month');
        $endtime   = time();
    }
    if (!empty($_GPC['time'])) {
        $starttime = strtotime($_GPC['time']['start']);
        $endtime   = strtotime($_GPC['time']['end']);
        if ($_GPC['searchtime'] == '1') {
            $condition .= " and addtime >= :starttime AND addtime <= :endtime ";
            $params[':starttime'] = $starttime;
            $params[':endtime']   = $endtime;
        }
    }  
	//echo var_dump($params);exit;
    $total1 = pdo_fetchcolumn("select count(*) from " . tablename('ewei_shop_orderflow') ." where 1=1 $condition1 $condition ", $params);
    $total2 = pdo_fetchcolumn("select count(*) from " . tablename('ewei_shop_orderflow') ." where 1=1 $condition2 $condition ", $params);
    $total=$total1+$total2;
    $pager = pagination($total, $pindex, $psize);
    $sql="select ordersn,price,remark,addtime,saluniacid as sid,remain from " . tablename('ewei_shop_orderflow') ." where 1=1 $condition1 $condition ";
    $sql.="  union all ";
    $sql.="select ordersn,-1*price as price,remark,addtime,supuniacid as sid,remain from " . tablename('ewei_shop_orderflow') ." where 1=1 $condition2 $condition ";
    $ssql="select * from ($sql) as tb1 $orderby LIMIT ". ($pindex - 1) * $psize . ',' . $psize;
    $list=pdo_fetchall($ssql, $params);
    foreach ($list as $rr=>$r)
    {	
		if(empty($r['sid']))$r['sid']=$uniacid ;
		$name=pdo_fetchcolumn("SELECT name FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$r['sid']}'");
			$list[$rr]['name']=$name;
		
    }
    $money=pdo_fetchcolumn("SELECT money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$uniacid}'");
    $idsql="SELECT supuniacid as sid FROM " . tablename('ewei_shop_orderflow') . " WHERE `saluniacid` = '{$uniacid}'";
    $idsql.="  union  ";
    $idsql.="SELECT saluniacid as sid FROM " . tablename('ewei_shop_orderflow') . " WHERE `supuniacid` = '{$uniacid}'";
    $pacid=pdo_fetch($idsql); 
	foreach($pacid as $p)
    {
        $pname[$p]=pdo_fetchcolumn("SELECT name FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$p}'");
    }
    $pname[$uniacid]="我的公众号";
}
load()->func('tpl');
include $this->template('web/finance/moneyflow');