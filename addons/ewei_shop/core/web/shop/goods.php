<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
$shopset  = m('common')->getSysset('shop');
$sql      = 'SELECT * FROM ' . tablename('ewei_shop_category') . ' WHERE `uniacid` = :uniacid ORDER BY `parentid`, `displayorder` DESC';
if(empty($_GPC['shopunid'])||$_GPC['shopunid']=="pp")
{
	$cw=array(':uniacid' => $_W['uniacid']);
}
else
{
	$cw=array(':uniacid' => $_GPC['shopunid']);
}
$category = pdo_fetchall($sql, $cw, 'id');
$parent   = $children = array();
if (!empty($category)) {
    foreach ($category as $cid => $cate) {
        if (!empty($cate['parentid'])) {
            $children[$cate['parentid']][] = $cate;
        } else {
            $parent[$cate['id']] = $cate;
        }
    }
}
$setting['isdispatch'] = pdo_fetchcolumn('select isdispatch  from ' . tablename('uni_settings') . ' where `uniacid` = :uniacid',
		array(':uniacid' => $_W['uniacid']));
$pv        = p('virtual');
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
if($operation=='service')
{
    if(empty($_GPC['id'])||!empty($_GPC['up'])){
        if($_GPC['submit'])       //提交新的记录
        {
            $can=array(
                ':uniacid'=>$_W['uniacid'],
                ':pcate'=>0,
                ':ccate'=>0,
                ':tcate'=>0
            );
            if (!empty($_GPC['category']['thirdid'])) {
                $set['tcate'] = intval($_GPC['category']['thirdid']);
            }
            if (!empty($_GPC['category']['childid'])) {
                $set['ccate'] = intval($_GPC['category']['childid']);
            }
            if (!empty($_GPC['category']['parentid'])) {
                $set['pcate'] = intval($_GPC['category']['parentid']);
            }
            if(empty($_GPC['category']['parentid'])&&empty($_GPC['category']['thirdid'])&&empty($_GPC['category']['childid']))
            {
              $sql="select count(*) from " .tablename('ewei_shop_cusservice') ." where uniacid=:uniacid and pcate=:pcate and ccate=:ccate and tcate=:tcate";
              if(pdo_fetchcolumn($sql,$can)>0)
              {
                  message('该分类已经有客服！',referer(), 'error');
              }       
            }
            if(!empty($_GPC['category']['parentid'])&&empty($_GPC['category']['thirdid'])&&empty($_GPC['category']['childid']))
            {
                $can[':pcate']=intval($_GPC['category']['parentid']);
                $sql="select count(*) from " .tablename('ewei_shop_cusservice') ." where uniacid=:uniacid and pcate=:pcate and ccate=:ccate and tcate=:tcate ";
                if(pdo_fetchcolumn($sql,$can)>0)
                {
                    message('该分类已经有客服！',referer(), 'error');
                }
            }
            if(!empty($_GPC['category']['parentid'])&&empty($_GPC['category']['thirdid'])&&!empty($_GPC['category']['childid']))
            {
                $can[':pcate']=intval($_GPC['category']['parentid']);
                $can[':ccate']=intval($_GPC['category']['childid']);
                $sql="select count(*) from " .tablename('ewei_shop_cusservice') ." where uniacid=:uniacid and pcate=:pcate and ccate=:ccate and tcate=:tcate ";           
                if(pdo_fetchcolumn($sql,$can)>0)
                {
                    message('该分类已经有客服！',referer(), 'error');
                }
            }
            if(!empty($_GPC['category']['parentid'])&&!empty($_GPC['category']['thirdid'])&&!empty($_GPC['category']['childid']))
            {
                $can[':pcate']=intval($_GPC['category']['parentid']);
                $can[':ccate']=intval($_GPC['category']['childid']);
                $can[':tcate']=intval($_GPC['category']['thirdid']);
                $sql="select count(*) from " .tablename('ewei_shop_cusservice') ." where uniacid=:uniacid and pcate=:pcate and ccate=:ccate and tcate=:tcate ";
                if(pdo_fetchcolumn($sql,$can)>0)
                {
                    message('该分类已经有客服！',referer(), 'error');
                }
            }
            $set['disable']=$_GPC['disable'];
            $set['uniacid']=$_W['uniacid'];
            $set['sername']=$_GPC['sername'];
            $set['position']=$_GPC['position'];
            $set['width']=$_GPC['width'].'px';
            $set['height']=$_GPC['height'].'px';
            $set['serviceurl']=$_GPC['serviceurl'];
            $set['thumb']=save_media($_GPC['thumb']);
            $set['createtime']=time();
            $set['updatetime']=time();
            if(empty($_GPC['id'])){
                if(pdo_insert('ewei_shop_cusservice', $set))
                {
                    message('插入成功！',referer(), 'success');
                }
            }
            else
            {
                $iid=intval($_GPC['id']);
                unset($set['uniacid']);
                unset($set['createtime']);
                if(pdo_update('ewei_shop_cusservice', $set,array('id' =>$iid,'uniacid'=>$_W['uniacid']))>0)
                {
                    message('更新成功！',$this->createWebUrl('shop/goods',array('op'=>'service')), 'success');
                }
            }
        }
    }
    else
    {
        $sid=intval($_GPC['id']);
        if($_GPC['ty']==1)   //查询更新
        {
            if(empty($_GPC['up'])){
             $item=pdo_fetch("select * from " .tablename('ewei_shop_cusservice') ." where uniacid=:uniacid and id=:id",
                array(
                    ':uniacid' =>  $_W['uniacid'],
                    ':id' =>  $sid
                ));
            }
            $item['width']=str_replace('px', '', $item['width']);
            $item['height']=str_replace('px', '', $item['height']);
        }
        if($_GPC['ty']==2)  //删除
        {
            if(pdo_query('delete from ' . tablename('ewei_shop_cusservice') . " where id=$sid and uniacid='{$_W['uniacid']}'")>0)
            {
                message('删除成功！',referer(), 'success');
            }
        }
    }
    
    if($_GPC['ty']!=1){
        $pindex = max(1, intval($_GPC['page']));
        $psize  = 10;
        $sql="select * from ".tablename('ewei_shop_cusservice')." where uniacid=:uniacid order by updatetime desc LIMIT ". ($pindex - 1) * $psize . ',' . $psize;
        $service = pdo_fetchall($sql, array(
            ':uniacid' =>  $_W['uniacid']
        ));
        $total = pdo_fetchcolumn("SELECT count(*) FROM " . tablename('ewei_shop_cusservice') . " WHERE uniacid = :uniacid ", array(
            ':uniacid' => $_W['uniacid']
        ));
        $pager = pagination($total, $pindex, $psize);
        foreach ($service as $rr=>&$r)
        {
         if(!empty($r['pcate'])){
             $r['npcate']=pdo_fetchcolumn("select name from " .tablename('ewei_shop_category') ." where uniacid=:uniacid and id=:pcate",
                array(
                 ':uniacid' =>  $_W['uniacid'],
                 ':pcate' =>  $r['pcate']
                 ));
            }
            if(!empty($r['ccate'])){
             $r['nccate']=pdo_fetchcolumn("select name from " .tablename('ewei_shop_category') ." where uniacid=:uniacid and id=:ccate",
                 array(
                 ':uniacid' =>  $_W['uniacid'],
                 ':ccate' =>  $r['ccate']
                ));
            }
            if(!empty($r['tcate'])){
            $r['ntcate']=pdo_fetchcolumn("select name from " .tablename('ewei_shop_category') ." where uniacid=:uniacid and id=:tcate",
                array(
                 ':uniacid' =>  $_W['uniacid'],
                 ':tcate' =>  $r['tcate']
                ));
            }
        }
    }
    
    
}
if($operation=='seruse')
{
    if(empty($_GPC['id']))
    {
        echo 0;exit;
    }
    else
    {
        $ssid=intval($_GPC['id']);
        $sql="select disable from ".tablename('ewei_shop_cusservice')." where id=:id ";
        $disable = pdo_fetchcolumn($sql, array(
            ':id'=>$ssid
        ));
        if(empty($disable))
        {
            if(pdo_update('ewei_shop_cusservice',array('disable'=>1), array('id' =>$ssid,'uniacid'=>$_W['uniacid']))>0)
            {
                echo 1;exit;
            }
            else {echo 0;exit;}
        }
        else
        {
            if(pdo_update('ewei_shop_cusservice',array('disable'=>0), array('id' =>$ssid,'uniacid'=>$_W['uniacid']))>0)
            {
               echo 2;exit;
            }
            else {echo 0;exit;}
        }
    }
}

if($operation=='downxls')
{
	 $ty=$_GPC['dty'].".xls";
	 $filename="../addons/ewei_shop/".$ty; //文件名 
	 Header( "Content-type:  application/octet-stream ");   
	 Header( "Accept-Ranges:  bytes ");  
	 Header( "Accept-Length: " .filesize($filename));  
	 header( "Content-Disposition:  attachment;  filename= $ty");   
	 echo file_get_contents($filename);  
	// readfile($filename);  		
}
if($operation=='option')
{

	$file=$_FILES['fileName'];
	$max_size="2000000";
	$fname=$file['name'];
	$ftype=strtolower(substr(strrchr($fname,'.'),1));
	//文件格式
	$uploadfile=$file['tmp_name'];

	if($_SERVER['REQUEST_METHOD']=='POST'){
		if(is_uploaded_file($uploadfile)){
			if($file['size']>$max_size){
				echo "Import file is too large";
				exit;
			}
			if($ftype!='xls'){
				echo "只能导入2003版,文件后缀格式必须为xls";
				exit;
			}
		}else{
			echo "文件名不能为空!";
			exit;
		}
	}
	include_once '../addons/feng_fightgroups/PHPExcel.php';
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objReader->setReadDataOnly(true);   //防止读到空的行
	try{
		$objPHPExcel = $objReader->load($uploadfile);
	}
	catch(Exception $e){
		echo "失败信息：".$e->getMessage();exit;
	}
	$sheet = $objPHPExcel->getSheet(0);
	$highestRow = $sheet->getHighestDataRow();
	$highestColumn = $sheet->getHighestDataColumn(); // 取得总列数
	$usucc_result=0;
	$error_result=0;
	for($j=2;$j<=$highestRow;$j++){
		for($k='A';$k<=$highestColumn;$k++){
			$key=$objPHPExcel->getActiveSheet()->getCell("$k"."1")->getValue();
			$data[$key][] =  (string)$objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue();
		}
	}
	$num=count($data['goodsid']);
	/*$option=array('goodsid','productprice','marketprice','costprice','stock','weight','goodssn','productsn','storecode');
	$spec=array('goodsid','spectitle');
	$specitem=array('title');*/
	for($i=0;$i<$num;$i++)
	{
		for($k='A';$k<=$highestColumn;$k++)
		{
			$key=$objPHPExcel->getActiveSheet()->getCell("$k"."1")->getValue();
			$option[$key]=$data[$key][$i];
		}
		$productsn=$data['productsn'][$i];
		$uniacid=$_W['uniacid'];
		$goodsid=$data['goodsid'][$i];
		$option['uniacid']=$_W['uniacid'];
		$id = pdo_fetch("select id from".tablename('ewei_shop_goods_option')."where productsn='{$productsn}' and uniacid=$uniacid and goodsid=$goodsid");
		if(empty($id))
		{
			message('批量修改失败-productsn:'.$productsn.'不存在！或goodsid有误');
		}
		else
		{
			$option['updatetime']=time();
			if(pdo_update('ewei_shop_goods_option',$option, array('productsn' =>$productsn,'uniacid'=>$uniacid,'goodsid'=>$goodsid)))
				$usucc_result++;
		}
		unset($goods);
	}
	$error_result=$highestRow-$usucc_result-1;
	message('批量修改商品规格成功,成功修改'.$usucc_result.'条,失败'.$error_result.'条', referer(), 'success');
}
if($operation=='batch')
{
	
	$file=$_FILES['fileName'];
	$max_size="2000000";
	$fname=$file['name'];
	$ftype=strtolower(substr(strrchr($fname,'.'),1));
	//文件格式
	$uploadfile=$file['tmp_name'];
	
	if($_SERVER['REQUEST_METHOD']=='POST'){
		if(is_uploaded_file($uploadfile)){
			if($file['size']>$max_size){
				echo "Import file is too large";
				exit;
			}
			if($ftype!='xls'){
					echo "只能导入2003版,文件后缀格式必须为xls";
				exit;
			}
		}else{
			echo "文件名不能为空!";
			exit;
		}
	}
	include_once '../addons/feng_fightgroups/PHPExcel.php';
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objReader->setReadDataOnly(true);   //防止读到空的行
	try{
          	$objPHPExcel = $objReader->load($uploadfile); 
    }
	catch(Exception $e){
 			echo "失败信息：".$e->getMessage();exit;
	}
	$sheet = $objPHPExcel->getSheet(0);
	$highestRow = $sheet->getHighestDataRow();
	$highestColumn = $sheet->getHighestDataColumn(); // 取得总列数
	$isucc_result=0;
	$usucc_result=0;
	$error_result=0;
	for($j=2;$j<=$highestRow;$j++){
		for($k='A';$k<=$highestColumn;$k++){
		$key=$objPHPExcel->getActiveSheet()->getCell("$k"."1")->getValue();
		$data[$key][] =  (string)$objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue();
		}	
	}
	//$num=count($data['productsn']);
	//echo $highestRow;exit;
	for($i=0;$i<$highestRow;$i++)
	{
		for($k='A';$k<=$highestColumn;$k++)
		{
			$key=$objPHPExcel->getActiveSheet()->getCell("$k"."1")->getValue();
			$goods[$key]=$data[$key][$i];
		}
		$goodsid=$data['id'][$i];
		$goods['uniacid']=$_W['uniacid'];
		//$id = pdo_fetch("select id from".tablename('ewei_shop_goods')."where productsn='{$productsn}'");
		if(empty($goodsid))
		{
			$goods['createtime']=time();
			if(pdo_insert('ewei_shop_goods', $goods))
			$isucc_result+=1;
		}
		else
		{
			$goods['updatetime']=time();
			if(pdo_update('ewei_shop_goods',$goods, array('id' =>$goodsid)))
			$usucc_result+=1;
		}	
		unset($goods);
	}
	$error_result=$highestRow-$usucc_result-$isucc_result-1;
	message('批量导入商品成功,成功插入'.$isucc_result.'条,成功更新'.$usucc_result.'条,失败'.$error_result.'条', referer(), 'success');
}
if ($operation == 'post') {
    session_id('oldurl');
    session_start();
    $id = intval($_GPC['id']);
    $_W['shopunid']=intval($_GPC['shopunid']);
    if (!empty($id)) {
        ca('shop.goods.edit|shop.goods.view');
    } else {
        ca('shop.goods.add');
    }
    $levels = m('member')->getLevels();
    $groups = m('member')->getGroups();
    if (!empty($id)) {
        $item = pdo_fetch("SELECT * FROM " . tablename('ewei_shop_goods') . " WHERE id = :id", array(
            ':id' => $id
        ));
        $parentstatus = pdo_fetchcolumn("SELECT status FROM " . tablename('ewei_shop_goods') . " WHERE id = :id", array(
        		':id' => $item['parentid']
        ));
        if (empty($item)) {
            message('抱歉，商品不存在或是已经删除！', '', 'error');
        }
        $noticetype = explode(',', $item['noticetype']);
        if ($shopset['catlevel'] == 3) {
            $cates = explode(',', $item['tcates']);
        } else {
            $cates = explode(',', $item['ccates']);
        }
        $discounts = json_decode($item['discounts'], true);
        if(empty($item['parentid']))
        {
       		 $allspecs  = pdo_fetchall("select * from " . tablename('ewei_shop_goods_spec') . " where goodsid=:id order by displayorder asc", array(
            	":id" => $id
        	));
        }
        else
        { 
        	$allspecs  = pdo_fetchall("select * from " . tablename('ewei_shop_goods_spec') . " where goodsid=:id order by displayorder asc", array(
        			":id" => $item['parentid']
        	));
        }
        foreach ($allspecs as &$s) {
            $s['items'] = pdo_fetchall("select a.id,a.specid,a.title,a.thumb,a.show,a.displayorder,a.valueId,a.virtual,b.title as title2 from " . tablename('ewei_shop_goods_spec_item') . " a left join " . tablename('ewei_shop_virtual_type') . " b on b.id=a.virtual  where a.specid=:specid order by a.displayorder asc", array(
                ":specid" => $s['id']
            ));
        }
        unset($s);
        $params   = pdo_fetchall("select * from " . tablename('ewei_shop_goods_param') . " where goodsid=:id order by displayorder asc", array(
            ':id' => $id
        ));
        $piclist1 = unserialize($item['thumb_url']);
        $piclist  = array();
        if (is_array($piclist1)) {
            foreach ($piclist1 as $p) {
                $piclist[] = is_array($p) ? $p['attachment'] : $p;
            }
        }
        $html    = "";
        $options = pdo_fetchall("select * from " . tablename('ewei_shop_goods_option') . " where goodsid=:id order by id asc", array(
            ':id' => $id
        ));
        
        $specs   = array();
        if (count($options) > 0) {
            $specitemids = explode("_", $options[0]['specs']);
            foreach ($specitemids as $itemid) {
                foreach ($allspecs as $ss) {
                    $items = $ss['items'];
                    foreach ($items as $it) {
                        if ($it['id'] == $itemid) {
                            $specs[] = $ss;
                            break;
                        }
                    }
                }
            }
            $html = '';
            $html .= '<table class="table table-bordered table-condensed">';
            $html .= '<thead>';
            $html .= '<tr class="active">';
            $len      = count($specs);
            $newlen   = 1;
            $h        = array();
            $rowspans = array();
            for ($i = 0; $i < $len; $i++) {
                $html .= "<th style='width:80px;'>" . $specs[$i]['title'] . "</th>";
                $itemlen = count($specs[$i]['items']);
                if ($itemlen <= 0) {
                    $itemlen = 1;
                }
                $newlen *= $itemlen;
                $h = array();
                for ($j = 0; $j < $newlen; $j++) {
                    $h[$i][$j] = array();
                }
                $l            = count($specs[$i]['items']);
                $rowspans[$i] = 1;
                for ($j = $i + 1; $j < $len; $j++) {
                    $rowspans[$i] *= count($specs[$j]['items']);
                }
            }
            $canedit = ce('shop.goods', $item);
             if ($canedit) {
                $html .= '<th class="info" style="width:120px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">库存</div><div class="input-group"><input type="text" class="form-control option_stock_all"  VALUE=""/><span class="input-group-addon"><a href="javascript:;" class="fa fa-hand-o-down" title="批量设置" onclick="setCol(\'option_stock\');"></a></span></div></div></th>';
                $html .= '<th class="success" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">销售价格</div><div class="input-group"><input type="text" class="form-control option_marketprice_all"  VALUE=""/><span class="input-group-addon"><a href="javascript:;" class="fa fa-hand-o-down" title="批量设置" onclick="setCol(\'option_marketprice\');"></a></span></div></div></th>';
                $html .= '<th class="warning" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">市场价格</div><div class="input-group"><input type="text" class="form-control option_productprice_all"  VALUE=""/><span class="input-group-addon"><a href="javascript:;" class="fa fa-hand-o-down" title="批量设置" onclick="setCol(\'option_productprice\');"></a></span></div></div></th>';
                $html .= '<th class="danger" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">成本价格</div><div class="input-group"><input type="text" class="form-control option_costprice_all"  VALUE=""/><span class="input-group-addon"><a href="javascript:;" class="fa fa-hand-o-down" title="批量设置" onclick="setCol(\'option_costprice\');"></a></span></div></div></th>';
                if(!empty($item['isdispatch'])){
                $html .= '<th class="primary" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">商品编码</div><div class="input-group"><input type="text" class="form-control option_goodssn_all"  VALUE=""/><span class="input-group-addon"><a href="javascript:;" class="fa fa-hand-o-down" title="批量设置" onclick="setCol(\'option_goodssn\');"></a></span></div></div></th>';                         
                }
                $html .= '<th class="danger" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">商品条码</div><div class="input-group"><input type="text" class="form-control option_productsn_all"  VALUE=""/><span class="input-group-addon"><a href="javascript:;" class="fa fa-hand-o-down" title="批量设置" onclick="setCol(\'option_productsn\');"></a></span></div></div></th>';
               if(!empty($item['isdispatch'])){
                $html .= '<th class="warning" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">商店编号</div><div class="input-group"><input type="text" class="form-control option_storecode_all"  VALUE=""/><span class="input-group-addon"><a href="javascript:;" class="fa fa-hand-o-down" title="批量设置" onclick="setCol(\'option_storecode\');"></a></span></div></div></th>';
               }
                $html .= '<th class="info" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">重量（克）</div><div class="input-group"><input type="text" class="form-control option_weight_all"  VALUE=""/><span class="input-group-addon"><a href="javascript:;" class="fa fa-hand-o-down" title="批量设置" onclick="setCol(\'option_weight\');"></a></span></div></div></th>';
                $html .= '</tr></thead>';
            } else {
                $html .= '<th class="info" style="width:120px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">库存</div></div></th>';
                $html .= '<th class="success" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">销售价格</div></div></th>';
                $html .= '<th class="warning" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">市场价格</div></div></th>';
                $html .= '<th class="danger" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">成本价格</div></div></th>';
                if(!empty($item['isdispatch'])){
                $html .= '<th class="primary" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">商品编码</div></div></th>';
                }
                $html .= '<th class="danger" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">商品条码</div></div></th>';
               if(!empty($item['isdispatch'])){
               	$html .= '<th class="warning" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">商店编号</div></div></th>';
               }
               	$html .= '<th class="info" style="width:130px;"><div class=""><div style="padding-bottom:10px;text-align:center;font-size:16px;">重量（克）</div></th>';
                $html .= '</tr></thead>';
            }
            for ($m = 0; $m < $len; $m++) {
                $k   = 0;
                $kid = 0;
                $n   = 0;
                for ($j = 0; $j < $newlen; $j++) {
                    $rowspan = $rowspans[$m];
                    if ($j % $rowspan == 0) {
                        $h[$m][$j] = array(
                            "html" => "<td rowspan='" . $rowspan . "'>" . $specs[$m]['items'][$kid]['title'] . "</td>",
                            "id" => $specs[$m]['items'][$kid]['id']
                        );
                    } else {
                        $h[$m][$j] = array(
                            "html" => "",
                            "id" => $specs[$m]['items'][$kid]['id']
                        );
                    }
                    $n++;
                    if ($n == $rowspan) {
                        $kid++;
                        if ($kid > count($specs[$m]['items']) - 1) {
                            $kid = 0;
                        }
                        $n = 0;
                    }
                }
            }
            $hh = "";
            for ($i = 0; $i < $newlen; $i++) {
                $hh .= "<tr>";
                $ids = array();
                for ($j = 0; $j < $len; $j++) {
                    $hh .= $h[$j][$i]['html'];
                    $ids[] = $h[$j][$i]['id'];
                }
                $ids = implode("_", $ids);
                $val = array(
                    "id" => "",
                    "title" => "",
                    "stock" => "",
                    "costprice" => "",
                    "productprice" => "",
                    "marketprice" => "",
                    "weight" => "",
                    'virtual' => ''
                );
                foreach ($options as $o) {
                    if ($ids === $o['specs']) {
                        $val = array(
                            "id" => $o['id'],
                            "title" => $o['title'],
                            "stock" => $o['stock'],
                            "costprice" => $o['costprice'],
                            "productprice" => $o['productprice'],
                            "marketprice" => $o['marketprice'],
                            "goodssn" => $o['goodssn'],
                            "productsn" => $o['productsn'],
                            "weight" => $o['weight'],
                            'virtual' => $o['virtual'],
							'storecode'=>$o['storecode']
                        );
                        break;
                    }
                }
                if ($canedit) {
                    $hh .= '<td class="info">';
                    $hh .= '<input name="option_stock_' . $ids . '[]"  type="text" class="form-control option_stock option_stock_' . $ids . '" value="' . $val['stock'] . '"/>';
                    $hh .= '<input name="option_id_' . $ids . '[]"  type="hidden" class="form-control option_id option_id_' . $ids . '" value="' . $val['id'] . '"/>';
                    $hh .= '<input name="option_ids[]"  type="hidden" class="form-control option_ids option_ids_' . $ids . '" value="' . $ids . '"/>';
                    $hh .= '<input name="option_title_' . $ids . '[]"  type="hidden" class="form-control option_title option_title_' . $ids . '" value="' . $val['title'] . '"/>';
                    $hh .= '<input name="option_virtual_' . $ids . '[]"  type="hidden" class="form-control option_title option_virtual_' . $ids . '" value="' . $val['virtual'] . '"/>';
                    $hh .= '</td>';
                    $hh .= '<td class="success"><input name="option_marketprice_' . $ids . '[]" type="text" class="form-control option_marketprice option_marketprice_' . $ids . '" value="' . $val['marketprice'] . '"/></td>';
                    $hh .= '<td class="warning"><input name="option_productprice_' . $ids . '[]" type="text" class="form-control option_productprice option_productprice_' . $ids . '" " value="' . $val['productprice'] . '"/></td>';
                    $hh .= '<td class="danger"><input name="option_costprice_' . $ids . '[]" type="text" class="form-control option_costprice option_costprice_' . $ids . '" " value="' . $val['costprice'] . '"/></td>';
                    if(!empty($item['isdispatch'])){
                    $hh .= '<td class="primary"><input name="option_goodssn_' . $ids . '[]" type="text" class="form-control option_goodssn option_goodssn_' . $ids . '" " value="' . $val['goodssn'] . '"/></td>';
                	}
                    $hh .= '<td class="danger"><input name="option_productsn_' . $ids . '[]" type="text" class="form-control option_productsn option_productsn_' . $ids . '" " value="' . $val['productsn'] . '"/></td>';
                    if(!empty($item['isdispatch'])){
                    $hh .= '<td class="warning"><input name="option_storecode_' . $ids . '[]" type="text" class="form-control option_storecode option_storecode_' . $ids . '" " value="' . $val['storecode'] . '"/></td>';
                    }
                    $hh .= '<td class="info"><input name="option_weight_' . $ids . '[]" type="text" class="form-control option_weight option_weight_' . $ids . '" " value="' . $val['weight'] . '"/></td>';
                    $hh .= '</tr>';
                } else {
                    $hh .= '<td class="info">' . $val['stock'] . '</td>';
                    $hh .= '<td class="success">' . $val['marketprice'] . '</td>';
                    $hh .= '<td class="warning">' . $val['productprice'] . '</td>';
                    $hh .= '<td class="danger">' . $val['costprice'] . '</td>';
                    if(!empty($item['isdispatch'])){
                    $hh .= '<td class="primary">' . $val['goodssn'] . '</td>';
                    }
                    $hh .= '<td class="danger">' . $val['productsn'] . '</td>';
                    if(!empty($item['isdispatch'])){
                    $hh .= '<td class="warning">' . $val['storecode'] . '</td>';
                    }
                    $hh .= '<td class="info">' . $val['weight'] . '</td>';
                    $hh .= '</tr>';
                }
            }
            $html .= $hh;
            $html .= "</table>";
        }
        if ($item['showlevels'] != '') {
            $item['showlevels'] = explode(',', $item['showlevels']);
        }
        if ($item['buylevels'] != '') {
            $item['buylevels'] = explode(',', $item['buylevels']);
        }
        if ($item['showgroups'] != '') {
            $item['showgroups'] = explode(',', $item['showgroups']);
        }
        if ($item['buygroups'] != '') {
            $item['buygroups'] = explode(',', $item['buygroups']);
        }
        $stores = array();
        if (!empty($item['storeids'])) {
            $stores = pdo_fetchall('select id,storename from ' . tablename('ewei_shop_store') . ' where id in (' . $item['storeids'] . ' ) and uniacid=' . $_W['uniacid']);
        }
        if (!empty($item['noticeopenid'])) {
            $saler = m('member')->getMember($item['noticeopenid']);
        }
    }
    if (empty($category)) {
        message('抱歉，请您先添加商品分类！', $this->createWebUrl('shop/category', array(
            'op' => 'post'
        )), 'error');
    }
    if (checksubmit('submit')) {
    	$pparentid = pdo_fetchcolumn("SELECT parentid FROM " . tablename('ewei_shop_goods') . " WHERE id = :id", array(
    			':id' => $id
    	));
        if (empty($_GPC['goodsname'])) {
            message('请输入商品名称！',$this->createWebUrl('shop/goods', array('op' => 'post','id' => $id,'shopunid'=>$_W['uniacid'])), 'error');
        }
        if (empty($_GPC['category']['parentid'])) {
            message('请选择商品分类！',$this->createWebUrl('shop/goods', array('op' => 'post','id' => $id,'shopunid'=>$_W['uniacid'])), 'error');
        }
        if (empty($_GPC['thumbs'])) {
            $_GPC['thumbs'] = array();
        }
        $data    = array(
            'unittype' => $_GPC['unittype'],
            'isproxy' => $_GPC['isproxy'],
			'storename' => $_GPC['storename'],
        	'storecode' => $_GPC['storecode'],
        	'isdispatch'=> $_GPC['isdispatch'],
        	'sharesale'=> $_GPC['sharesale'],
            'uniacid' => intval($_W['uniacid']),
            'displayorder' => intval($_GPC['displayorder']),
            'title' => trim($_GPC['goodsname']),
            'pcate' => intval($_GPC['category']['parentid']),
            'ccate' => intval($_GPC['category']['childid']),
            'tcate' => intval($_GPC['category']['thirdid']),
            'thumb' => save_media($_GPC['thumb']),
            'type' => intval($_GPC['type']),
            'isrecommand' => intval($_GPC['isrecommand']),
            'ishot' => intval($_GPC['ishot']),
            'isnew' => intval($_GPC['isnew']),
            'isdiscount' => intval($_GPC['isdiscount']),
            'issendfree' => intval($_GPC['issendfree']),
            'isnodiscount' => intval($_GPC['isnodiscount']),
            'istime' => intval($_GPC['istime']),
            'timestart' => strtotime($_GPC['timestart']),
            'timeend' => strtotime($_GPC['timeend']),
            'description' => trim($_GPC['description']),
            'goodssn' => trim($_GPC['goodssn']),
            'unit' => trim($_GPC['unit']),
            'createtime' => TIMESTAMP,
            'total' => intval($_GPC['total']),
            'totalcnf' => intval($_GPC['totalcnf']),
            'marketprice' => $_GPC['marketprice'],
            'weight' => $_GPC['weight'],
            'costprice' => $_GPC['costprice'],
            'productprice' => trim($_GPC['productprice']),
            'productsn' => trim($_GPC['productsn']),
            'credit' => trim($_GPC['credit']),
            'maxbuy' => intval($_GPC['maxbuy']),
            'usermaxbuy' => intval($_GPC['usermaxbuy']),
            'hasoption' => intval($_GPC['hasoption']),
            'sales' => intval($_GPC['sales']),
            'share_icon' => trim($_GPC['share_icon']),
            'share_title' => trim($_GPC['share_title']),
            'cash' => intval($_GPC['cash']),
            'status' => intval($_GPC['status']),
            'showlevels' => is_array($_GPC['showlevels']) ? implode(",", $_GPC['showlevels']) : '',
            'buylevels' => is_array($_GPC['buylevels']) ? implode(",", $_GPC['buylevels']) : '',
            'showgroups' => is_array($_GPC['showgroups']) ? implode(",", $_GPC['showgroups']) : '',
            'buygroups' => is_array($_GPC['buygroups']) ? implode(",", $_GPC['buygroups']) : '',
            'isverify' => intval($_GPC['isverify']),
            'storeids' => is_array($_GPC['storeids']) ? implode(',', $_GPC['storeids']) : '',
            'noticeopenid' => trim($_GPC['noticeopenid']),
            'noticetype' => is_array($_GPC['noticetype']) ? implode(",", $_GPC['noticetype']) : '',
            'needfollow' => intval($_GPC['needfollow']),
            'followurl' => trim($_GPC['followurl']),
            'followtip' => trim($_GPC['followtip']),
            'deduct' => $_GPC['deduct'],
            'virtual' => intval($_GPC['type']) == 3 ? intval($_GPC['virtual']) : 0,
            'discounts' => is_array($_GPC['discounts']) ? json_encode($_GPC['discounts']) : array(),
            'detail_logo' => save_media($_GPC['detail_logo']),
            'detail_shopname' => trim($_GPC['detail_shopname']),
            'detail_totaltitle' => trim($_GPC['detail_totaltitle']),
            'detail_btntext1' => trim($_GPC['detail_btntext1']),
            'detail_btnurl1' => trim($_GPC['detail_btnurl1']),
            'detail_btntext2' => trim($_GPC['detail_btntext2']),
            'detail_btnurl2' => trim($_GPC['detail_btnurl2'])
        );
        $cateset = m('common')->getSysset('shop');
        $pcates  = array();
        $ccates  = array();
        $tcates  = array();
        if (is_array($_GPC['cates'])) {
            $postcates = $_GPC['cates'];
            foreach ($postcates as $pid) {
                if ($cateset['catlevel'] == 3) {
                    $tcate    = pdo_fetch('select id ,parentid from ' . tablename('ewei_shop_category') . ' where id=:id and uniacid=:uniacid limit 1', array(
                        ':id' => $pid,
                        ':uniacid' => $_W['uniacid']
                    ));
                    $ccate    = pdo_fetch('select id ,parentid from ' . tablename('ewei_shop_category') . ' where id=:id and uniacid=:uniacid limit 1', array(
                        ':id' => $tcate['parentid'],
                        ':uniacid' => $_W['uniacid']
                    ));
                    $tcates[] = $tcate['id'];
                    $ccates[] = $ccate['id'];
                    $pcates[] = $ccate['parentid'];
                } else {
                    $ccate    = pdo_fetch('select id ,parentid from ' . tablename('ewei_shop_category') . ' where id=:id and uniacid=:uniacid limit 1', array(
                        ':id' => $pid,
                        ':uniacid' => $_W['uniacid']
                    ));
                    $ccates[] = $ccate['id'];
                    $pcates[] = $ccate['parentid'];
                }
            }
        }
        $data['pcates'] = implode(',', $pcates);
        $data['ccates'] = implode(',', $ccates);
        $data['tcates'] = implode(',', $tcates);
        $content        = htmlspecialchars_decode($_GPC['content']);
        preg_match_all("/<img.*?src=[\'| \"](.*?(?:[\.gif|\.jpg|\.png|\.jpeg]?))[\'|\"].*?[\/]?>/", $content, $imgs);
        $images = array();
        if (isset($imgs[1])) {
            foreach ($imgs[1] as $img) {
                $im       = array(
                    "old" => $img,
                    "new" => save_media($img)
                );
                $images[] = $im;
            }
        }
        foreach ($images as $img) {
            $content = str_replace($img['old'], $img['new'], $content);
        }
        $data['content'] = $content;
        if (p('commission')) {
            $cset = p('commission')->getSet();
            if (!empty($cset['level'])) {
                $data['nocommission']     = intval($_GPC['nocommission']);
                $data['hascommission']    = intval($_GPC['hascommission']);
                $data['hidecommission']   = intval($_GPC['hidecommission']);
                $data['commission1_rate'] = $_GPC['commission1_rate'];
                $data['commission2_rate'] = $_GPC['commission2_rate'];
                $data['commission3_rate'] = $_GPC['commission3_rate'];
                $data['commission1_pay']  = $_GPC['commission1_pay'];
                $data['commission2_pay']  = $_GPC['commission2_pay'];
                $data['commission3_pay']  = $_GPC['commission3_pay'];
                $data['commission_thumb'] = save_media($_GPC['commission_thumb']);
            }
        }
        if ($data['total'] === -1) {
            $data['total']    = 0;
            $data['totalcnf'] = 2;
        }
        if (is_array($_GPC['thumbs'])) {
            $thumbs    = $_GPC['thumbs'];
            $thumb_url = array();
            foreach ($thumbs as $th) {
                $thumb_url[] = save_media($th);
            }
            $data['thumb_url'] = serialize($thumb_url);
        }
        if (empty($id)) {
            pdo_insert('ewei_shop_goods', $data);
            $id = pdo_insertid();
            plog('shop.goods.add', "添加商品 ID: {$id}");
        } else {
            unset($data['createtime']);
            pdo_update('ewei_shop_goods', $data, array(
                'id' => $id
            ));
			if(empty($pparentid))  //更改销售商对应的商品
			{                  		
            	$pdata['status']=0;
            	$pdata['pupdatetime']=time();  
            	$pdata['costprice']=$data['costprice'];
           		 pdo_update('ewei_shop_goods', $pdata, array(
            		'parentid' => $id
            	));
			}
            plog('shop.goods.edit', "编辑商品 ID: {$id}");
        }
        $totalstocks         = 0;
        $param_ids           = $_POST['param_id'];
        $param_titles        = $_POST['param_title'];
        $param_values        = $_POST['param_value'];
        $param_displayorders = $_POST['param_displayorder'];
        $len                 = count($param_ids);
        $paramids            = array();
        for ($k = 0; $k < $len; $k++) {
            $param_id     = "";
            $get_param_id = $param_ids[$k];
            $a            = array(
                "uniacid" => $_W['uniacid'],
                "title" => $param_titles[$k],
                "value" => $param_values[$k],
                "displayorder" => $k,
                "goodsid" => $id
            );
            if (!is_numeric($get_param_id)) {
                pdo_insert("ewei_shop_goods_param", $a);
                $param_id = pdo_insertid();
            } else {
                pdo_update('ewei_shop_goods_param', $a, array(
                    'id' => $get_param_id
                ));
                $param_id = $get_param_id;
            }
            $paramids[] = $param_id;
        }
        if (count($paramids) > 0) {
            pdo_query("delete from " . tablename('ewei_shop_goods_param') . " where goodsid=$id and id not in ( " . implode(',', $paramids) . ")");
        } else {
            pdo_query('delete from ' . tablename('ewei_shop_goods_param') . " where goodsid=$id");
        }
        if(empty($pparentid)){  
        $files       = $_FILES;
        $spec_ids    = $_POST['spec_id'];
        $spec_titles = $_POST['spec_title'];
        $specids     = array();
        $len         = count($spec_ids);
        $specids     = array();
        $spec_items  = array();
        for ($k = 0; $k < $len; $k++) {
            $spec_id     = "";
            $get_spec_id = $spec_ids[$k];
            $a           = array(
                "uniacid" => $_W['uniacid'],
                "goodsid" => $id,
                "displayorder" => $k,
                "title" => $spec_titles[$get_spec_id]
            );
            if (is_numeric($get_spec_id)) {
                pdo_update("ewei_shop_goods_spec", $a, array(
                    "id" => $get_spec_id
                ));
                $spec_id = $get_spec_id;
            } else {
                pdo_insert('ewei_shop_goods_spec', $a);
                $spec_id = pdo_insertid();
            }
            $spec_item_ids       = $_POST["spec_item_id_" . $get_spec_id];
            $spec_item_titles    = $_POST["spec_item_title_" . $get_spec_id];
            $spec_item_shows     = $_POST["spec_item_show_" . $get_spec_id];
           $spec_item_thumbs    = $_POST["spec_item_thumb_" . $get_spec_id];
            $spec_item_oldthumbs = $_POST["spec_item_oldthumb_" . $get_spec_id];
            $spec_item_virtuals  = $_POST["spec_item_virtual_" . $get_spec_id];
            $itemlen             = count($spec_item_ids);
            $itemids             = array();
            for ($n = 0; $n < $itemlen; $n++) {
                $item_id     = "";
                $get_item_id = $spec_item_ids[$n];
                $d           = array(
                    "uniacid" => $_W['uniacid'],
                    "specid" => $spec_id,
                    "displayorder" => $n,
                    "title" => $spec_item_titles[$n],
                    "show" => $spec_item_shows[$n],
                    "thumb" => save_media($spec_item_thumbs[$n]),
                    "virtual" => $data['type'] == 3 ? $spec_item_virtuals[$n] : 0
                );
                $f           = "spec_item_thumb_" . $get_item_id;
                if (is_numeric($get_item_id)) {
                    pdo_update("ewei_shop_goods_spec_item", $d, array(
                        "id" => $get_item_id
                    ));
                    $item_id = $get_item_id;
                } else {
                    pdo_insert('ewei_shop_goods_spec_item', $d);
                    $item_id = pdo_insertid();
                }
                $itemids[]    = $item_id;
                $d['get_id']  = $get_item_id;
                $d['id']      = $item_id;
                $spec_items[] = $d;
            }
            if (count($itemids) > 0) {
                pdo_query("delete from " . tablename('ewei_shop_goods_spec_item') . " where uniacid={$_W['uniacid']} and specid=$spec_id and id not in (" . implode(",", $itemids) . ")");
            } else {
                pdo_query('delete from ' . tablename('ewei_shop_goods_spec_item') . " where uniacid={$_W['uniacid']} and specid=$spec_id");
            }
            pdo_update('ewei_shop_goods_spec', array(
                'content' => serialize($itemids)
            ), array(
                "id" => $spec_id
            ));
            $specids[] = $spec_id;
        }
        if (count($specids) > 0) {
            pdo_query("delete from " . tablename('ewei_shop_goods_spec') . " where uniacid={$_W['uniacid']} and goodsid=$id and id not in (" . implode(",", $specids) . ")");
        } else {
            pdo_query('delete from ' . tablename('ewei_shop_goods_spec') . " where uniacid={$_W['uniacid']} and goodsid=$id");
        }
        }
        $option_idss          = $_POST['option_ids'];
        $option_productprices = $_POST['option_productprice'];
        $option_marketprices  = $_POST['option_marketprice'];
        $option_costprices    = $_POST['option_costprice'];
        $option_stocks        = $_POST['option_stock'];
        $option_weights       = $_POST['option_weight'];
        $option_goodssns      = $_POST['option_goodssn'];
        $option_productssns   = $_POST['option_productsn'];
		$option_storecode     = $_POST['option_storecode'];
        $len                  = count($option_idss);
        $optionids            = array();
        for ($k = 0; $k < $len; $k++) {
            $option_id     = "";
            $ids           = $option_idss[$k];
            $get_option_id = $_GPC['option_id_' . $ids][0];
            $idsarr        = explode("_", $ids);
            $newids        = array();
            foreach ($idsarr as $key => $ida) {
                foreach ($spec_items as $it) {
                    if ($it['get_id'] == $ida) {
                        $newids[] = $it['id'];
                        break;
                    }
                }
            }
            $newids = implode("_", $newids);
            $a      = array(
                "uniacid" => $_W['uniacid'],
                "title" => $_GPC['option_title_' . $ids][0],
                "productprice" => $_GPC['option_productprice_' . $ids][0],
                "costprice" => $_GPC['option_costprice_' . $ids][0],
                "marketprice" => $_GPC['option_marketprice_' . $ids][0],
                "stock" => $_GPC['option_stock_' . $ids][0],
                "weight" => $_GPC['option_weight_' . $ids][0],
                "goodssn" => $_GPC['option_goodssn_' . $ids][0],
                "productsn" => $_GPC['option_productsn_' . $ids][0],
				"storecode" => $_GPC['option_storecode_' . $ids][0],
                "goodsid" => $id,
                "specs" => $newids,
                'virtual' => $data['type'] == 3 ? $_GPC['option_virtual_' . $ids][0] : 0

            );
            $totalstocks += $a['stock'];
            if (empty($get_option_id)) {
                pdo_insert("ewei_shop_goods_option", $a);
                $option_id = pdo_insertid();
            } else {
            	if(!empty($pparentid)){
            	unset($a['specs']);
            	}
                pdo_update('ewei_shop_goods_option', $a, array(
                    'id' => $get_option_id
                ));
                $option_id = $get_option_id;
            }
            $optionids[] = $option_id;
        }
        if (count($optionids) > 0) {
            pdo_query("delete from " . tablename('ewei_shop_goods_option') . " where goodsid=$id and id not in ( " . implode(',', $optionids) . ")");
        } else {
            pdo_query('delete from ' . tablename('ewei_shop_goods_option') . " where goodsid=$id");
        }
        pdo_query('delete from ' . tablename('ewei_shop_goods_option') . " where parentid=$id");
        if ($data['type'] == 3 && $pv) {
            $pv->updateGoodsStock($id);
        } else {
            if (($totalstocks > 0) && ($data['totalcnf'] != 2)) {
                pdo_update("ewei_shop_goods", array(
                    "total" => $totalstocks
                ), array(
                    "id" => $id
                ));
            }
        }
        message('商品更新成功！', $_SESSION['oldurl'], 'success');
    }
    if (p('commission')) {
        $com_set = p('commission')->getSet();
    }
    if ($pv) {
        $virtual_types = pdo_fetchall("select * from " . tablename('ewei_shop_virtual_type') . " where uniacid=:uniacid order by id asc", array(
            ":uniacid" => $_W['uniacid']
        ));
    }
    $levels  = m('member')->getLevels();
    $details = pdo_fetchall('select detail_logo,detail_shopname,detail_btntext1, detail_btnurl1 ,detail_btntext2,detail_btnurl2,detail_totaltitle from ' . tablename('ewei_shop_goods') . " where uniacid=:uniacid and detail_shopname<>''", array(
        ':uniacid' => $_W['uniacid']
    ));
    foreach ($details as &$d) {
        $d['detail_logo_url'] = tomedia($d['detail_logo']);
    }
    unset($d);
} elseif ($operation == 'display') {
    ca('shop.goods.view');
    session_id("oldurl");
    session_start(); 
    $_SESSION['oldurl']="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    if (!empty($_GPC['displayorder'])) {
        ca('shop.goods.edit');
        foreach ($_GPC['displayorder'] as $id => $displayorder) {
            pdo_update('ewei_shop_goods', array(
                'displayorder' => $displayorder
            ), array(
                'id' => $id
            ));
        }
        plog('shop.goods.edit', '批量修改商品排序');
        message('商品排序更新成功！', $this->createWebUrl('shop/goods', array(
            'op' => 'display'
        )), 'success');
    }
    $pindex    = max(1, intval($_GPC['page']));
    $psize     = 20;
    $condition = ' WHERE `uniacid` = :uniacid AND `deleted` = :deleted';
    $params    = array(
        ':uniacid' => $_W['uniacid'],
        ':deleted' => '0'
    );
    if (!empty($_GPC['keyword'])) {
        $_GPC['keyword'] = trim($_GPC['keyword']);
        $condition .= ' AND `title` LIKE :title';
        $params[':title'] = '%' . trim($_GPC['keyword']) . '%';
    }
    if (!empty($_GPC['category']['thirdid'])) {
        $condition .= ' AND `tcate` = :tcate';
        $params[':tcate'] = intval($_GPC['category']['thirdid']);
    }
    if (!empty($_GPC['category']['childid'])) {
        $condition .= ' AND `ccate` = :ccate';
        $params[':ccate'] = intval($_GPC['category']['childid']);
    }
    if (!empty($_GPC['category']['parentid'])) {
        $condition .= ' AND `pcate` = :pcate';
        $params[':pcate'] = intval($_GPC['category']['parentid']);
    }
    if(!isset($_GPC['allup']))
    {
    	if (isset($_GPC['status'])) {
        	$condition .= ' AND `status` = :status';
        	$params[':status'] = intval($_GPC['status']);
    	}
    }
    $_W['shopunid']=$_W['uniacid'];
    $sort="";
    $ps=1;
    if($_GPC['shopunid']!="pp")
    {
    	$condition .= ' AND parentid=0';
    }
    if (isset($_GPC['shopunid'])) {
    	if($_GPC['shopunid']=="pp")
    	{   		
    		$condition .= ' AND parentid<>0';
    		$sort=" pupdatetime desc, ";
    		if($_GPC['psort']==1)
    		{
    			 $ps=2;
    			$condition .= " AND pupdatetime > updatetime ";
    		}
    		if($_GPC['psort']==2)
    		{
    			 $ps=1;
    			$condition .= " AND pupdatetime <= updatetime ";
    		}
    	}   	
    	else
    	{  		
    		$params[':uniacid']=intval($_GPC['shopunid']);
    		$_W['shopunid']=intval($_GPC['shopunid']);
    		if ($_GPC['shopunid']!=$_W['uniacid']) {
    			$condition .= ' AND `sharesale` = 1 and isdispatch = 0';
    		}
    	}
    }
    if(isset($_GPC['allup']))
    {
		$condition .= " AND pupdatetime <= updatetime ";
		$upid="";
		$where="";
		$uppid =pdo_fetchall('SELECT parentid,id FROM ' . tablename('ewei_shop_goods') .$condition ,$params);
    	if($_GPC['shopunid']=="pp"){
    		foreach($uppid as $rr=>$row)
    		{
    			$upst=pdo_fetchcolumn('SELECT status FROM ' . tablename('ewei_shop_goods') ." where id=".$row['parentid']);
				if($_GPC['allup']==1)   //选择上架时
				{
					if($upst==1)$upid .=$row['id'].",";     //商家上架的商品
				}
				else
				{
					$upid .=$row['id'].",";      //下架不判断
				}
    		}
			if(!empty($upid)){
				$upid="(".substr($upid, 0,-1).")"; 
				$where=" where id in ".$upid ;	
			}			
    	}
		else
		{
			foreach($uppid as $rr=>$row)
    		{
				$upid .=$row['id'].",";
    		}

			$upid="(".substr($upid, 0,-1).")"; 
			if($_GPC['allup']==1) $where=" where id in ".$upid ;
			else $where=" where id in ".$upid ." or parentid in ".$upid;
		}
		$upsql="update ".tablename('ewei_shop_goods')." set status=".$_GPC['allup'].$where;
		if(!empty($uppid)&&!empty($upid))pdo_query($upsql);
    }
    $shtml="<select name='shopunid' class='form-control' onchange='cclear(this)'>";
    $sql   = 'SELECT COUNT(*) FROM ' . tablename('ewei_shop_goods') . $condition;
    $total = pdo_fetchcolumn($sql, $params);
    $sql   = 'SELECT uniacid,name FROM ' . tablename('uni_account');  //uni_account公众号表
    $account = pdo_fetchall($sql);
	if(!empty($_GPC['dwnid']))
	{
		ob_end_clean();
		header('Content-type: text/html; charset=utf-8');
		header("Content-type:application/vnd.ms-excel;charset=UTF-8"); 
		header("Content-Disposition:filename=id.xls");
		$sql   = 'SELECT id,title FROM ' . tablename('ewei_shop_goods') . $condition;
		$dwngid = pdo_fetchall($sql, $params);
		$tgid=iconv('UTF-8', 'GB2312',"商品ID");
		$tgtitle=iconv('UTF-8', 'GB2312',"商品标题");
		$tgpsn=iconv('UTF-8', 'GB2312',"商品条码");
		$tgpt=iconv('UTF-8', 'GB2312',"规格名");
		if($_GPC['dwnid']==1)
		{
		    echo $tgid."\t".$tgtitle."\n";
			foreach($dwngid as $rr=>$row)
			{
				echo $row['id']."\t";
				echo iconv('UTF-8', 'GB2312', $row['title'])."\n";
			}
			exit;			
		}
		else
		{
			echo $tgid."\t".$tgpsn."\t".$tgpt."\t".$tgtitle."\n";
			foreach($dwngid as $rr=>$row)
			{
				$dwnption = pdo_fetchall("SELECT goodsid,productsn,title from " . tablename('ewei_shop_goods_option') . " WHERE goodsid = :goodsid", array(
    		    			':goodsid' => $row['id']));
				foreach($dwnption as $pt=>$ptt)
				{
					echo $ptt['goodsid']."\t";
					echo $ptt['productsn']."\t";
					echo iconv('UTF-8', 'GB2312',$ptt['title'])."\t";
					echo iconv('UTF-8', 'GB2312', $row['title'])."\n";
				}
			}	
			exit;			
		}
	}
    foreach($account as $rrow=>$row)
    {
    	
    	$unid=$row['uniacid'];
    	$name=$row['name'];
    	if($row['uniacid']==$_W['uniacid'])$shtml.='<option value="' . $unid . '"' . (($unid == $params[':uniacid']) ? 'selected="selected"' : '') . '>我的商品</option>';
    	else $shtml.='<option value="' . $unid . '"' . (($unid == $params[':uniacid']) ? 'selected="selected"' : '') . '>' .$name . '</option>';
    	//()里面的是php语句。因为不在''里面.
    }
    $shtml.="<option value='pp'" . (($_GPC['shopunid']=='pp')? 'selected="selected"' : '') .">已选择其他商家商品</option>";
    $shtml.="</select>";
    if (!empty($total)) {
        $sql   = 'SELECT * FROM ' . tablename('ewei_shop_goods') . $condition . ' ORDER BY '. $sort .'`status` DESC, `displayorder` DESC,
						`id` DESC LIMIT ' . ($pindex - 1) * $psize . ',' . $psize;
        $list  = pdo_fetchall($sql, $params);
        $pager = pagination($total, $pindex, $psize);
    }
    $sql2='SELECT count(id) FROM ' . tablename('ewei_shop_goods') . " WHERE uniacid = :uniacid AND deleted = 0 and pupdatetime > updatetime";
    $changeid =pdo_fetchcolumn($sql2 , array(":uniacid"=>$_W['uniacid']));
    if($changeid>0)
    {
    	$chhtml="（提示：商品供应商改了商品！）--共：".$changeid."个。";
    	
    }
 
} elseif ($operation == 'delete') {
    ca('shop.goods.delete');
    $id  = intval($_GPC['id']);
    $row = pdo_fetch("SELECT id, title, thumb FROM " . tablename('ewei_shop_goods') . " WHERE id = :id", array(
        ':id' => $id
    ));
    if (empty($row)) {
        message('抱歉，商品不存在或是已经被删除！');
    }
    pdo_update('ewei_shop_goods', array(
        'deleted' => 1
    ), array(
        'id' => $id
    ));
	if(!empty($id)){
    pdo_update('ewei_shop_goods', array(
    		'deleted' => 1
    ), array(
    		'parentid' => $id
    ));
	}
    plog('shop.goods.delete', "删除商品 ID: {$id} 标题: {$row['title']} ");
    message('删除成功！', referer(), 'success');
} elseif ($operation == 'setgoodsproperty') {
    ca('shop.goods.edit');
	if($_GPC['id']=='all')$id='all';
	else if($_GPC['id']=='upd')$id='upd';
    else $id=intval($_GPC['id']);
    $type = $_GPC['type'];
    $data = intval($_GPC['data']);
    if (in_array($type, array(
        'new',
        'hot',
        'recommand',
        'discount',
        'time',
        'sendfree',
        'nodiscount'
    ))) {
        $data = ($data == 1 ? '0' : '1');
        pdo_update('ewei_shop_goods', array(
            'is' . $type => $data
        ), array(
            "id" => $id,
            "uniacid" => $_W['uniacid']
        ));
        if ($type == 'new') {
            $typestr = "新品";
        } else if ($type == 'hot') {
            $typestr = "热卖";
        } else if ($type == 'recommand') {
            $typestr = "推荐";
        } else if ($type == 'discount') {
            $typestr = "促销";
        } else if ($type == 'time') {
            $typestr = "限时卖";
        } else if ($type == 'sendfree') {
            $typestr = "包邮";
        } else if ($type == 'nodiscount') {
            $typestr = "不参与折扣状态";
        }
        plog('shop.goods.edit', "修改商品{$typestr}状态   ID: {$id}");
        die(json_encode(array(
            'result' => 1,
            'data' => $data
        )));
    }
    if (in_array($type, array(
        'status'
    ))) {
        $data = ($data == 1 ? '0' : '1');
        pdo_update('ewei_shop_goods', array(
            $type => $data
        ), array(
            "id" => $id,
            "uniacid" => $_W['uniacid']
        ));
        plog('shop.goods.edit', "修改商品上下架状态   ID: {$id}");
        die(json_encode(array(
            'result' => 1,
            'data' => $data
        )));
    }
    if (in_array($type, array(
        'type'
    ))) {
        $data = ($data == 1 ? '2' : '1');
        pdo_update('ewei_shop_goods', array(
            $type => $data
        ), array(
            "id" => $id,
            "uniacid" => $_W['uniacid']
        ));
        plog('shop.goods.edit', "修改商品类型   ID: {$id}");
        die(json_encode(array(
            'result' => 1,
            'data' => $data
        )));
    }
    if (in_array($type, array(
    		'choice'
    ))) {		
    	$choice=array();
		$params =array();
    	$dd = ($data == 1 ? '0' : '1'); //单个选
    	if($id=='all'||$id=='upd')$dd=$data;       //全选
    	$chgoodsid[0]['id']=$id;
    	if($id=='all'||$id=='upd'){
  			unset($chgoodsid);
  			$var=explode("&", urldecode($_GPC['url']));
  			foreach ($var as $row)
  			{
  				$rr=explode("=", $row);
  				$_GPC[$rr[0]]=$rr[1];
  			}
			if($id=='all'){
				$condition = ' WHERE `uniacid` = :uniacid AND `deleted` = 0 AND `parentid`=0 AND `sharesale` = 1 and `isdispatch` = 0';
				$params    = array(':uniacid' => $_GPC['shopunid']);
			}
			else
			{
				$condition = ' WHERE `uniacid` = :uniacid AND `deleted` = 0  AND parentid<>0 AND pupdatetime > updatetime';
				$params    = array(':uniacid' => $_W['uniacid']);
			}
    		if (!empty($_GPC['keyword'])) {
    			$_GPC['keyword'] = trim($_GPC['keyword']);
    			$condition .= ' AND `title` LIKE :title';
    			$params[':title'] = '%' . trim($_GPC['keyword']) . '%';
    		}
    		if (!empty($_GPC['category[thirdid]'])) {
    			$condition .= ' AND `tcate` = :tcate';
    			$params[':tcate'] = intval($_GPC['category[thirdid]']);
    		}
    		if (!empty($_GPC['category[childid]'])) {
    			$condition .= ' AND `ccate` = :ccate';
    			$params[':ccate'] = intval($_GPC['category[childid]']);
    		}
    		if (!empty($_GPC['category[parentid]'])) {
    			$condition .= ' AND `pcate` = :pcate';
    			$params[':pcate'] = intval($_GPC['category[parentid]']);
    		}
    		if (isset($_GPC['status'])) {
    			$condition .= ' AND `status` = :status';
    			$params[':status'] = intval($_GPC['status']);
    		}
			if($id=='all')
    		$chgoodsid = pdo_fetchall('select id from ' . tablename('ewei_shop_goods') . $condition,$params);
			else
			$chgoodsid = pdo_fetchall('select parentid as id from ' . tablename('ewei_shop_goods') . $condition,$params);
    	}
		
		$tt=isset($_GPC['tt'])?intval($_GPC['tt']):0;
    	if(!empty($chgoodsid)&&$tt!=1){
    		foreach ($chgoodsid as $rr=>$cgid){
    			$choiceunid = pdo_fetchcolumn('select choiceunid  from ' . tablename('ewei_shop_goods') . ' where `id` = :id',array(':id' =>$cgid['id']));
    			if(!empty($choiceunid))$choice=unserialize($choiceunid);
    			$choice[$_W['uniacid']]=$dd;
    			$schoice=serialize($choice);
    			pdo_update('ewei_shop_goods', array('choiceunid'=>$schoice), array("id" => $cgid['id']));
    			}
    	}  

    	if($dd==1)
    	{
    		foreach ($chgoodsid as $rr=>$cgid){
    		$gooddata = pdo_fetch("SELECT * from " . tablename('ewei_shop_goods') . " WHERE id = :id", array(
    			':id' => $cgid['id']));
    		$gooddata['uniacid']=$_W['uniacid'];
    		$gooddata['parentid']=$cgid['id'];
    		$gooddata['updatetime']=time();
    		$gooddata['isdispatch']=0;
    		$gooddata['sharesale']=0;
    		$gooddata['status']=0;    		   		
    		$pcategory = pdo_fetch("SELECT * from " . tablename('ewei_shop_category') . " WHERE id = :id", array(
    				':id' => $gooddata['pcate']    				
    		));
    		$ccategory = pdo_fetch("SELECT * from " . tablename('ewei_shop_category') . " WHERE id = :id", array(
    				':id' => $gooddata['ccate']
    		));
    		$tcategory = pdo_fetch("SELECT * from " . tablename('ewei_shop_category') . " WHERE id = :id", array(
    				':id' => $gooddata['tcate']
    		));
			$pcategory['updatetime']=time();
			$ccategory['updatetime']=time();
			$tcategory['updatetime']=time();
    		$pgoryid = pdo_fetchcolumn('select id  from ' . tablename('ewei_shop_category') . ' where `choiceid` = :choiceid and `uniacid`=:uniacid',
    				array(':choiceid' => $gooddata['pcate'] ,':uniacid' => $_W['uniacid'] ));
    		if(empty($pgoryid))
    		{    	
    			$pcategory['uniacid']=$_W['uniacid'];
    			$pcategory['choiceid']=$pcategory['id'];
    			unset($pcategory['id']);
    			pdo_insert('ewei_shop_category', $pcategory);
    			$pginsid=pdo_insertid();
    			$gooddata['pcate']=$pginsid;
    		}
    		else
    		{
    			unset($pcategory['id']);
    			unset($pcategory['uniacid']);
    			unset($pcategory['choiceid']);
    			$gooddata['pcate']=$pgoryid;
    			pdo_update('ewei_shop_category',$pcategory, array('id'=>$gooddata['pcate']));
    		}
    		$cgoryid = pdo_fetchcolumn('select id  from ' . tablename('ewei_shop_category') . ' where `choiceid` = :choiceid and `uniacid`=:uniacid',
    				array(':choiceid' => $gooddata['ccate'] ,':uniacid' => $_W['uniacid'] ));
    		if(empty($cgoryid))
    		{
    			$ccategory['uniacid']=$_W['uniacid'];
    			$ccategory['parentid']=empty($pgoryid)?$pginsid:$pgoryid;
    			$ccategory['choiceid']=$ccategory['id'];
    			unset($ccategory['id']);
    			pdo_insert('ewei_shop_category', $ccategory);
    			$cginsid=pdo_insertid();
    			$gooddata['ccate']=$cginsid;
    		}
    		else
    		{
    			unset($ccategory['id']);
    			unset($ccategory['uniacid']);
    			unset($ccategory['parentid']);
    			unset($ccategory['choiceid']);
    			$gooddata['ccate']=$cgoryid;
    			pdo_update('ewei_shop_category',$ccategory, array('id'=>$gooddata['ccate']));
    		}
    		if(!empty($gooddata['tcate'])){
    			$tgoryid = pdo_fetchcolumn('select id  from ' . tablename('ewei_shop_category') . ' where `choiceid` = :choiceid and `uniacid`=:uniacid',
    				array(':choiceid' => $gooddata['tcate'] ,':uniacid' => $_W['uniacid'] ));
    			if(empty($tgoryid))
    			{
    				$tcategory['uniacid']=$_W['uniacid'];
    				$tcategory['parentid']=empty($cgoryid)?$cginsid:$cgoryid;
    				$tcategory['choiceid']=$tcategory['id'];
    				unset($tcategory['id']);
    				pdo_insert('ewei_shop_category', $tcategory);
    				$tginsid=pdo_insertid();
    				$gooddata['tcate']=$tginsid;
    			}
    			else
    			{
    				unset($tcategory['id']);
    				unset($tcategory['uniacid']);
    				unset($tcategory['parentid']);
    				unset($tcategory['choiceid']);
    				$gooddata['tcate']=$tgoryid;
    				pdo_update('ewei_shop_category',$tcategory, array('id'=>$gooddata['tcate']));
    			}
    		}
    		unset($gooddata['choiceunid']);   //暂时没用
    		unset($gooddata['id']);
    		$sum = pdo_fetchcolumn('select id  from ' . tablename('ewei_shop_goods') . ' where `parentid` = :parentid and `uniacid`=:uniacid',
    			array(':parentid' => $cgid['id'],':uniacid' => $_W['uniacid'] ));
    		if(empty($sum))
    		{
    			pdo_insert('ewei_shop_goods', $gooddata);
    			$insid=pdo_insertid();
    			if($gooddata['hasoption'])
    			{
    				$goption = pdo_fetchall("SELECT * from " . tablename('ewei_shop_goods_option') . " WHERE goodsid = :goodsid", array(
    				':goodsid' => $cgid['id']));
    				if(!empty($goption))
    				{  				
    					foreach($goption as $ngp=>$gp)
    					{
    						$gp['uniacid']=$_W['uniacid'];
    						$gp['goodsid']=$insid;
    						$gp['updatetime']=time();
    						$gp['parentid']=$gp['id'];
    						unset($gp['id']);
    						pdo_insert('ewei_shop_goods_option', $gp);
    					}
    				}	
    			}
    		}
    		else
    		{
    		    pdo_update('ewei_shop_goods',$gooddata, array('id'=>$sum));
    		    if($gooddata['hasoption'])
    		    {
    		    	$goption = pdo_fetchall("SELECT * from " . tablename('ewei_shop_goods_option') . " WHERE goodsid = :goodsid", array(
    		    			':goodsid' => $cgid['id']));
    		    	$parentid1 = pdo_fetchall("SELECT parentid from " . tablename('ewei_shop_goods_option') . " WHERE goodsid = :goodsid", array(
    		    			':goodsid' => $sum));
    		    	$parentid2 = pdo_fetchall("SELECT id from " . tablename('ewei_shop_goods_option') . " WHERE goodsid = :goodsid", array(
    		    			':goodsid' => $cgid['id']));
    		  		foreach ($parentid2 as $pp2)
    		  		{
    		  			$pid2[]=$pp2['id'];
    		  		}
    		  		$remain=array();
    		    	foreach($parentid1 as $npid=>$pid)
    		    	{
    		    		$pid1=$pid['parentid'];
    		    		if(in_array($pid['parentid'], $pid2))
    		    		{
    		    			$remain[]=$pid['parentid'];
    		    		
    		    		}
    		    		else
    		    		{
    		    			pdo_query('delete from ' . tablename('ewei_shop_goods_option') . " where goodsid=$sum and parentid=$pid1");
    		    		}
    		    	}
    		    	if(!empty($goption))
    		    	{
    		    		foreach($goption as $gp1=>$gp)
    		    		{
    		    			$gp['uniacid']=$_W['uniacid'];
    		    			$gp['goodsid']=$sum;
    		    			$gp['updatetime']=time();
    		    			$gp['parentid']=$gp['id'];
    		    			unset($gp['id']);  
    		    			if(in_array($gp['parentid'], $remain)){    		    			  		    		
    		    				pdo_update('ewei_shop_goods_option',$gp, array('goodsid'=>$sum,'parentid'=>$gp['parentid']));
    		    			}
    		    			else{
    		    			pdo_insert('ewei_shop_goods_option', $gp);
    		    			}
    		    		}
    		    	}
    		    }
    		}
    	  }
    	}
    	else
    	{	foreach ($chgoodsid as $rr=>$cgid){
    			pdo_update('ewei_shop_goods', array('deleted' => 1,'status'=> 0), array('parentid' => $cgid['id'],'uniacid' => $_W['uniacid'] ));
    		}
    	}
    	$chhtml="";
    	$sql2='SELECT count(id) FROM ' . tablename('ewei_shop_goods') . " WHERE uniacid = :uniacid AND deleted = 0 and pupdatetime > updatetime";
    	$changeid =pdo_fetchcolumn($sql2 , array(":uniacid"=>$_W['uniacid']));
    	if($changeid>0)
    	{
    		$chhtml="（提示：商品供应商改了商品！）--共：".$changeid."个。";            //当选择了之后动态改变 变化的数量
    		 
    	}
    	die(json_encode(array('result' => 1,'data' =>$dd,'chhtml'=>$chhtml)));
    }
    die(json_encode(array('result' => 0)));
    
}
load()->func('tpl');
include $this->template('web/shop/goods');