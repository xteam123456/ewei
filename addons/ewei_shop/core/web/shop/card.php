<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
ca('shop.card.view');
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
load()->model('user');
$uniacid=$_W['uniacid'];
if (empty($starttime) || empty($endtime)) {
    $starttime = strtotime('-1 month');
    $endtime   = time();
}
if ($operation == 'display') {
    $pindex = max(1, intval($_GPC['page']));
    $psize  = 20;
    if(!empty($_GPC['secardid']))
    {
       $conditon.=" and id={$_GPC['secardid']}";
    }
    if (empty($starttime) || empty($endtime)) {
        $starttime = strtotime('-1 month');
        $endtime   = time();
    }
    $goods = pdo_fetchall("SELECT id,title,marketprice FROM " . tablename('ewei_shop_goods') . " WHERE uniacid = :uniacid and deleted=0 and hasoption=0", array(
        ':uniacid' => $uniacid
    ));
    $typecard = pdo_fetchall("SELECT * FROM " . tablename('ewei_shop_typecard') . " WHERE uniacid = :uniacid and status=1 $conditon order by id desc LIMIT ". ($pindex - 1) * $psize . ',' . $psize , array(
        ':uniacid' => $uniacid
    ));
    $total = pdo_fetchcolumn("SELECT count(*) FROM " . tablename('ewei_shop_typecard') . " WHERE uniacid = :uniacid and status=1 $conditon", array(
        ':uniacid' => $uniacid
    ));
    $pager = pagination($total, $pindex, $psize);   
    
}
if ($operation == 'addtype') {
    if (empty($starttime) || empty($endtime)) {
        $starttime = strtotime('-1 month');
        $endtime   = time();
    }
    if (!empty($_GPC['time'])) {
        $starttime = strtotime($_GPC['time']['start']);
        $endtime   = strtotime($_GPC['time']['end']);
    }
    $typecard['title']=$_GPC['title'];
    $typecard['cdmoney']=floatval($_GPC['cdmoney']);
    $typecard['title']=$_GPC['title'];
    $typecard['goodsid']=$_GPC['goodsid'];
    $typecard['createtime']=$starttime;
    $typecard['endtime']=$endtime;
    $typecard['uniacid']=$uniacid;
    if(!empty($_GPC['ucard']))
    {
        
        if(pdo_update('ewei_shop_typecard', $typecard, array('id' => $_GPC['id']))>0){
            message('更新成功！',$this->createWebUrl('shop/card', array('op' => 'display')), 'success');
        }
        else
        {
            message('更新失败！',$this->createWebUrl('shop/card', array('op' => 'display' )));
        }
    }
    else
    {
        if(pdo_insert('ewei_shop_typecard', $typecard)>0){
            message('添加成功！',$this->createWebUrl('shop/card', array('op' => 'display')), 'success');
        }
        else
        {
            message('添加失败！',$this->createWebUrl('shop/card', array('op' => 'display' )));
        }
    }
}
if ($operation == 'update') {
    $tcard = pdo_fetch("SELECT * FROM " . tablename('ewei_shop_typecard') . " WHERE id = :id ", array(
        ':id' => $_GPC['id']
    ));
    $num = pdo_fetchcolumn("SELECT count(*) FROM " . tablename('ewei_shop_newcard') . " WHERE typeid = :typeid ", array(
        ':typeid' => $_GPC['id']
    ));  //是否已经发行
    $con=array(':uniacid' => $uniacid);
    $where=" WHERE uniacid = :uniacid and deleted=0  and hasoption=0 ";
    if($num>0)
    {
        $con['goodsid']= $tcard['goodsid'];
        $where=" WHERE uniacid = :uniacid and deleted=0  and hasoption=0 and id = :goodsid" ;
    }
    $goods = pdo_fetchall("SELECT id,title,marketprice  FROM " . tablename('ewei_shop_goods') . $where, $con);
    $starttime =$tcard['createtime'];
    $endtime   =$tcard['endtime'];
}
if ($operation == 'delete') {
        if(pdo_update('ewei_shop_typecard', array('status'=>2), array('id' => $_GPC['id']))>0)
        {
            message('删除成功！',$this->createWebUrl('shop/card', array('op' => 'display')), 'success');
        }
        else
        {
            message('删除失败！',$this->createWebUrl('shop/card', array('op' => 'display' )));
        }
}
if ($operation == 'chcard') {
    if(pdo_update('ewei_shop_newcard', array('status'=>$_GPC['stu']), array('id' => $_GPC['id']))>0)
    {
        message('操作成功！',$this->createWebUrl('shop/card', array('op' => 'newcard','id'=>$_GPC['tyid'])), 'success');
    }
    else
    {
        message('操作失败！',$this->createWebUrl('shop/card', array('op' => 'newcard','id'=>$_GPC['tyid'])));
    }
}
if ($operation == 'newcard') 
{
    $parm=array(':uniacid' => $uniacid,':typeid'=>$_GPC['id'] );
    $where="  WHERE 1=1 and tb1.uniacid = :uniacid and tb1.typeid=:typeid ";
    if(!empty($_GPC['ordersn']))
    {
        $parm[':ordersn']=$_GPC['ordersn'];
        $where .=" and tb1.ordersn=:ordersn ";
    }
    if (!empty($_GPC['time'])) {
        $parm[':createtime'] = strtotime($_GPC['time']['start']);
        $parm[':endtime']   = strtotime($_GPC['time']['end']);
        $starttime = $parm[':createtime'];
        $endtime   = $parm[':endtime'];
        $where .=" and tb2.endtime>=:createtime and tb2.endtime<=:endtime ";
    }
    if(isset($_GPC['ttyid'])&&$_GPC['ttyid']!=99)
    {
        $parm[':status']=$_GPC['ttyid'];
        $where .=" and tb1.status=:status ";
    }
    $pindex = max(1, intval($_GPC['page']));
    $psize  = 20;
    if(!empty($_GPC['output']))$pp='';
    else $pp=" limit ".($pindex - 1) * $psize . ',' . $psize;
    $sql="SELECT tb1.*,tb2.title,tb2.createtime,tb2.endtime,tb2.status as tstatus FROM " . tablename('ewei_shop_newcard') . " as tb1 left join ". tablename('ewei_shop_typecard')." as tb2 on tb1.typeid=tb2.id ";
    $sql .="  $where order by tb1.id ". $pp;
    $sql2="SELECT count(*) FROM " . tablename('ewei_shop_newcard') . " as tb1 left join ". tablename('ewei_shop_typecard') ." as tb2 on tb1.typeid=tb2.id ";
    $sql2 .=$where; 
    $newcard = pdo_fetchall($sql, $parm);
    foreach ($newcard as $row=>$rr)
    {
        if(!empty($rr['openid']))
        {
            $nickname[$rr['openid']] = pdo_fetchcolumn("SELECT nickname FROM " . tablename('mc_mapping_fans') . " WHERE openid=:openid ", array(
                ':openid' => $rr['openid']
            ));
            if(empty($nickname[$rr['openid']]))$nickname[$rr['openid']]=$rr['openid'];
        }
    }
    $total = pdo_fetchcolumn($sql2, array(
        ':uniacid' => $uniacid,
        ':typeid'=>$_GPC['id']
    ));
    $pager = pagination($total, $pindex, $psize);
    if(!empty($_GPC['output'])){  
        header('Content-type: text/html; charset=utf-8');
        header("Content-type:application/vnd.ms-excel;charset=UTF-8");
        header("Content-Disposition:filename=card.xls");
        $cdid=iconv('UTF-8', 'GB2312',"编号");
        $cdtitle=iconv('UTF-8', 'GB2312',"现金券类型");
        $cdstatus=iconv('UTF-8', 'GB2312',"是否使用");
        $cdorsn=iconv('UTF-8', 'GB2312',"订单号");
        $cduser=iconv('UTF-8', 'GB2312',"使用会员");
        $cdcwp=iconv('UTF-8', 'GB2312',"卷码");
        $cdstime=iconv('UTF-8', 'GB2312',"开始时间");
        $cdetime=iconv('UTF-8', 'GB2312',"结束时间");
        echo $cdid."\t".$cdtitle."\t".$cdstatus."\t".$cdorsn."\t".$cduser."\t".$cdcwp."\t".$cdstime."\t".$cdetime."\n";
        foreach($newcard as $rr=>$row)
        {       
            if(empty($row['status']))
            {
                $row['status']="未使用";              
            }
            else if($row['status']==1)
            {
                $row['status']="已核销";
            }
            else
            {
                $row['status']="无效";
            }
            echo $row['id']."\t";
            echo iconv('UTF-8', 'GB2312',$row['title'])."\t";       
            echo iconv('UTF-8', 'GB2312',$row['status'])."\t";
            echo $row['ordersn']."\t";
			echo iconv('UTF-8', 'GB2312',$nickname[$row['openid']])."\t";
			echo $row['cpwd']."\t";
			echo date('Y-m-d H:i',$row['createtime'])."\t";
			echo date('Y-m-d H:i',$row['endtime'])."\n";
        }
        exit;
    }
}
if ($operation == 'createcard')
{

    $card = pdo_fetchall("SELECT id,title,cdmoney FROM " . tablename('ewei_shop_typecard') . " WHERE uniacid = :uniacid and status=1 order by id desc", array(
        ':uniacid' => $uniacid
    ));
   $allnum=intval($_GPC['allnum']);
   $len=strlen(strval($allnum));
    if(!empty($allnum))
    {
        $cpw=array();
        $oldnum = pdo_fetchcolumn("SELECT allnum FROM " . tablename('ewei_shop_typecard') . " WHERE uniacid = :uniacid  and id=:id", array(
            ':uniacid' => $uniacid,
            ':id' => $_GPC['cardid']
        ));
        $tt=1;
        if($oldnum>0)
        {
            $check = pdo_fetch("SELECT cpwd,id FROM " . tablename('ewei_shop_newcard') . " WHERE uniacid = :uniacid  and typeid=:typeid order by id desc limit 1", array(
                ':uniacid' => $uniacid,
                ':typeid' => $_GPC['cardid']
            ));
            if(!empty($check)){
                $tt=intval(substr($check['cpwd'],-7,1)+1);    //多于10次的时候会循环回去
                if($tt==10)
                {
                    $tt=0;
                }
            } 
        }
        $newnum= $oldnum+$allnum;
        if(pdo_update('ewei_shop_typecard', array('allnum'=>$newnum), array('id' => $_GPC['cardid']))>0)
        {
            $ll='%0'.$len.'d';
            for($i=1;$i<=$allnum;$i++){
                if(empty($check)){
                    $newcard['id']=$_GPC['cardid'].sprintf($ll, $i);
                }
                else 
                {
                    $newcard['id']=intval($check['id']+$i);
                }
                $newcard['uniacid']=$uniacid;
                $newcard['typeid']= $_GPC['cardid'];
                $str = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVW";
                $rand = '';
                for ($j = 0; $j < 6; $j++) {
                   $rand .= $str[mt_rand(0, strlen($str)-1)];
                }
				$ran=strtolower($rand);
                while(in_array($ran,$cpw))
                {
                    for ($j = 0; $j < 6; $j++) {
                         $rand .= $str[mt_rand(0, strlen($str)-1)];
                    }
					$ran=strtolower($rand);					
                }         
                $cpw[]=$ran;
                $newcard['cpwd']=$_GPC['cardid'].$tt.$rand;    //后面6位随机，前面位类型号
                pdo_insert('ewei_shop_newcard', $newcard);
            }
            unset($cpw);
            message('发放成功！',$this->createWebUrl('shop/card', array('op' => 'display')), 'success');
        }
        else
        {
            message('发放失败！',$this->createWebUrl('shop/card', array('op' => 'display' )));
        }
    }
}
load()->func('tpl');
include $this->template('web/shop/card');