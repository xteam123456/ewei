<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
$openid = m('user')->getOpenid();
$member = m('member')->getInfo($openid);

if(($_GPC['mid']&&$_GPC['gid']&&$_GPC['level'])&&($_GPC['mid']!=$member['id'])){
	$ggid=$_GPC['gid'];
	$pmember = m('member')->getMember($_GPC['mid'], true);

	$authorid = pdo_fetchcolumn('select authorid from ' . tablename('ewei_shop_authgoods') . ' where uniacid=:uniacid and goodsid=:goodsid and openid=:openid order by createtime desc limit 1', array(
      ':uniacid' =>$_W['uniacid'],
      ':goodsid' =>$_GPC['gid'],
      ':openid'  =>$pmember['openid']
	));
	$oppid=$pmember['openid'];
	$level=intval($_GPC['level']);
	$has = pdo_fetchcolumn('select count(*) from ' . tablename('ewei_shop_authgoods') . ' where uniacid=:uniacid and goodsid=:goodsid and openid=:openid', array(
			':uniacid' =>$_W['uniacid'],
			':goodsid' =>$_GPC['gid'],
			':openid'  =>$openid
			));
			
}
if ($_W['isajax']) {
    if ($_W['ispost']) {
        $memberdata = $_GPC['memberdata'];
        pdo_update('ewei_shop_member', $memberdata, array(
            'openid' => $openid,
            'uniacid' => $_W['uniacid']
        ));
        if (!empty($member['uid'])) {
            $mcdata = $_GPC['mcdata'];
            load()->model('mc');
            mc_update($member['uid'], $mcdata);
        }
		if(empty($_GPC['daili']))
		{
			show_json(1);
		}
		else
		{
			$auth['uniacid']=$_W['uniacid'];
			$auth['createtime']=time();		
			$auth['openid']=$openid;
			$auth['status']=2;			   //申请
			$auth['userid']=$member['id'];
			
			$author = pdo_fetch('select goodsid,title from ' . tablename('ewei_shop_authgoods') . ' where uniacid=:uniacid and goodsid=:goodsid and openid=:openid', array(
			':uniacid' =>$_W['uniacid'],
			':goodsid' =>$_GPC['ggid'],
			':openid'  =>$_GPC['oppid']
			));
			
			$auth['goodsid']=$author['goodsid'];
			$auth['title']=$author['title'];
			pdo_insert('ewei_shop_authgoods', $auth);
			$level=$_GPC['level'];
			pdo_update("ewei_shop_member",array('level'=>$level),array(
				'uniacid' =>$_W['uniacid'],
				'openid'=>$openid				
			));			
			show_json(2);
		}
    }
    show_json(1, array(
        'member' => $member	
    ));
}
include $this->template('member/info');