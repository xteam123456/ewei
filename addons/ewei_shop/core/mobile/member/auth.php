<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$openid    = m('user')->getOpenid();
$uniacid   = $_W['uniacid'];
$id=empty($_GPC['id'])?0:$_GPC['id'];
$shd=empty($_GPC['shd'])?0:$_GPC['shd'];
$level = m('member')->getLevel($openid);
$levels=m('member')->getLevels($level['level']);
$levelcount=count($levels);
if ($_W['isajax']) {
    if ($operation == 'display') {
		if(empty($_GPC['id'])){
		 $member = m('member')->getMember($openid, true);
		$level  = array(
        'levelname' => empty($set['shop']['levelname']) ? '普通会员' : $set['shop']['levelname']
		);
		if (!empty($member['level'])) {
			$level = m('member')->getLevel($openid);
		}
		
		
        $pindex    = max(1, intval($_GPC['page']));
        $psize     = 10;
        $condition = ' and f.uniacid = :uniacid and f.openid=:openid and f.status=1';
        $params    = array(
            ':uniacid' => $_W['uniacid'],
            ':openid' => $openid
        );
        $sql       = 'SELECT COUNT(*) FROM ' . tablename('ewei_shop_authgoods') . " f where 1 {$condition}";
        $total     = pdo_fetchcolumn($sql, $params);
        $list      = array();
        if (!empty($total)) {
            $sql  = 'SELECT f.*,f.goodsid,f.status,g.thumb FROM ' . tablename('ewei_shop_authgoods') . ' f ' . ' left join ' . tablename('ewei_shop_category') . ' g on f.goodsid = g.id ' . ' where 1 ' . $condition . ' ORDER BY `id` DESC LIMIT ' . ($pindex - 1) * $psize . ',' . $psize;
            $list = pdo_fetchall($sql, $params);
            $list = set_medias($list, 'thumb');
			foreach($list as $ll=>&$l)
			{
				if(time()>$l['starttime']&&time()<$l['endtime'])
				{
					$l['ss']=1;
				}
				else
				{
					$l['ss']=2;
				}
			}
        }
		
		$return=array(
			'level' => $level,			
            'total' => $total,
            'list' => $list,
            'pagesize' => $psize
        );
		if(!empty($_GPC['shd']))
		{
			$return['shd']=1;
			
		}
			show_json(1, $return);
		}
		else
		{
			header("location:".$this->createPluginMobileUrl('poster/build')."&openid=".$openid ."&gid=".$_GPC['id'] );
						
		}
    }
	
}
include $this->template('member/auth');