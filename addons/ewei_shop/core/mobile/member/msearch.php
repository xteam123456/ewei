<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
if ($_W['isajax']) {
    if ($operation == 'display') {
        $membercon='';
        $list =array();
        if(!empty($_GPC['keyword']))
        {
            $membercon = " and (weixin ='".$_GPC['keyword']."' or mobile='".$_GPC['keyword']."') ";
            $member = pdo_fetch("SELECT openid,mobile,realname,nickname,id FROM " . tablename('ewei_shop_member') . " WHERE uniacid=:uniacid  $membercon ", array(
                ':uniacid' => $_W['uniacid']
            ));

            if(!empty($member['openid'])){
                $member['level'] = m('member')->getLevel($member['openid'])['levelname'];
                $member['company'] = '广州市尚医生物科技有限公司';
                $list = pdo_fetch("SELECT goodsid,title,endtime FROM " . tablename('ewei_shop_authgoods') . " WHERE uniacid=:uniacid  and openid=:openid and status=1", array(
                    ':uniacid' => $_W['uniacid'],
                    ':openid' =>$member['openid']
                ));
                $list['endtime']=date("Y-m-d",$list['endtime']);
                //$a=print_r($list,true);
                //file_put_contents("tt.txt",$a);
                if($list){
                    show_json(1, array(
                        "member"=>$member,
                        "list" =>$list,
                        "total" =>1
                    ));
                }
                else
                {
                    show_json(1,array('total'=>0));
                }
            }
            else
            {
                show_json(1,array('total'=>0));
            }
        }
        else
        {
            show_json(1,array('total'=>0));
        }
    }
}
include $this->template('member/msearch');