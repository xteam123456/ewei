<?php


if (!defined('IN_IA')) {
    exit('Access Denied');
}

function put_status($ordersn,$code,$status)
{
	if(!empty($ordersn)&&!empty($code)&&!empty($status))
	{
		$socket = new \Thrift\Transport\TSocket('120.24.58.216', 28888);     # 建立socket
		$framedSocket = new \Thrift\Transport\TFramedTransport($socket); #这个要和服务器使用的一致
		$transport = $framedSocket;
		$protocol = new \Thrift\Protocol\TCompactProtocol($transport);   # 这里也要和服务器使用的协议一致
		$channel_protocol = new \Thrift\Protocol\TMultiplexedProtocol($protocol, 'channel');
		$channel_client= new \syncs\channel\OrderServiceClient($channel_protocol);  # 构造客户端
		try {
			$transport->open();
			$return=$channel_client->putOrderStatus(new \syncs\channel\ChannelContext(array("code"=>"$code", "signature"=>'ASDSAKDLJ!@#*(!@#(*(DUASKD12389KXA')),
				"$ordersn","$status");
			$transport->close();                       # 关闭链接
		}
		catch (Exception $e)
		{
			$d=print_r($e,true);
			file_put_contents("put_statuslog.txt",'订单号:'.$ordersn.'||错误信息:'.$d);			
		}
	}
	else
	{
		file_put_contents("put_statuslog.txt",'订单号：'.$ordersn.'||错误信息:传入的参数为空，无法提交');
	}
	
}

global $_W, $_GPC;
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$openid    = m('user')->getOpenid();
if (empty($openid)) {
    $openid = $_GPC['openid'];
}
$member  = m('member')->getMember($openid);
$uniacid = $_W['uniacid'];
$orderid = intval($_GPC['orderid']);
$xshordersn = $_GPC['xshordersn'];
if ($operation == 'display' && $_W['isajax']) {
	if(empty($xshordersn)){
    	if (empty($orderid)) {
       	 	show_json(0, '参数错误!');
    	}
    	$order = pdo_fetch("select * from " . tablename('ewei_shop_order') . ' where id=:id and uniacid=:uniacid and openid=:openid limit 1', array(
        	':id' => $orderid,
        	':uniacid' => $uniacid,
        	':openid' => $openid
    	));
    	if (empty($order)) {
        	show_json(0, '订单未找到!');
    	}
    	if ($order['status'] == -1) {
        	show_json(-1, '订单已关闭, 无法付款!');
   	 	} else if ($order['status'] >= 1) {
       	 	show_json(-1, '订单已付款, 无需重复支付!');
    	}
    	$log = pdo_fetch('SELECT * FROM ' . tablename('core_paylog') . ' WHERE `uniacid`=:uniacid AND `module`=:module AND `tid`=:tid limit 1', array(
       	 	':uniacid' => $uniacid,
        	':module' => 'ewei_shop',
        	':tid' => $order['ordersn']
    	));
    	if (!empty($log) && $log['status'] != '0') {
       	 	show_json(-1, '订单已支付, 无需重复支付!');
    	}
    	if (!empty($log) && $log['status'] == '0') {
        	pdo_delete('core_paylog', array(
           	 'plid' => $log['plid']
        	));
        	$log = null;
    	}
    	$plid = $log['plid'];
    	if (empty($log)) {
        	$log = array(
            	'uniacid' => $uniacid,
            	'openid' => $member['uid'],
            	'module' => "ewei_shop",
            	'tid' => $order['ordersn'],
            	'fee' => $order['price'],
            	'status' => 0
        	);
       	 	pdo_insert('core_paylog', $log);
         	$plid = pdo_insertid();
    	}
	}
	else
	{
		$xshorder = pdo_fetchall("select * from " . tablename('ewei_shop_order') . ' where xshordersn=:xshordersn and uniacid=:uniacid and openid=:openid', array(
				':xshordersn' => $xshordersn,
				':uniacid' => $uniacid,
				':openid' => $openid
		));
		if (empty($xshorder)) {
			show_json(0, '订单未找到!');
		}
		$order['price']=0;
		$order['xshordersn']=$xshordersn;
		foreach($xshorder as $row=>$xorder){
			$log = array(
					'uniacid' => $uniacid,
					'openid' => $member['uid'],
					'module' => "ewei_shop",
					'tid' => $xorder['ordersn'],
					'fee' => $xorder['price'],
					'status' => 0
			);
			$order['price']=floatval($order['price']+$xorder['price']);
			pdo_insert('core_paylog', $log);
			//$plid = pdo_insertid();
		}
	}
    $set           = m('common')->getSysset(array(
        	'shop',
        	'pay'
    ));
    $credit        = array(
        'success' => false
    );
    $currentcredit = 0;
    if (isset($set['pay']) && $set['pay']['credit'] == 1) {
			
        if ($order['deductcredit2'] <= 0) {
            $credit = array(
                'success' => true,
                'current' => m('member')->getCredit($openid, 'credit2')
            );
			
        }
    }
    load()->model('payment');
    $setting = uni_setting($_W['uniacid'], array(
        'payment'
    ));
    $wechat  = array(
        'success' => false
    );
    if (is_weixin()) {
        if (isset($set['pay']) && $set['pay']['weixin'] == 1) {
            if (is_array($setting['payment']['wechat']) && $setting['payment']['wechat']['switch']) {
                $wechat['success'] = true;
            }
        }
    }
    $alipay = array(
        'success' => false
    );
    if (isset($set['pay']) && $set['pay']['alipay'] == 1) {
        if (is_array($setting['payment']['alipay']) && $setting['payment']['alipay']['switch']) {
            $alipay['success'] = true;
        }
    }
    $unionpay = array(
        'success' => false
    );
    if (isset($set['pay']) && $set['pay']['unionpay'] == 1) {
        if (is_array($setting['payment']['unionpay']) && $setting['payment']['unionpay']['switch']) {
            $unionpay['success'] = true;
        }
    }
    $cash      = array(
        'success' => $order['cash'] == 1 && isset($set['pay']) && $set['pay']['cash'] == 1
    );
    if(empty($xshordersn)){
   	 	$returnurl = urlencode($this->createMobileUrl('order/pay', array('orderid' => $orderid)));
    }
    else
    {
    	$returnurl = urlencode($this->createMobileUrl('order/pay', array('xshordersn' => $xshordersn)));
    }
    show_json(1, array(
        'order' => $order,
		'xsh'=>$xshordersn,
        'set' => $set,
        'credit' => $credit,
        'wechat' => $wechat,
        'alipay' => $alipay,
        'unionpay' => $unionpay,
        'cash' => $cash,
        'isweixin' => is_weixin(),
        'currentcredit' => $currentcredit,
        'returnurl' => $returnurl
    ));
} else if ($operation == 'pay' && $_W['ispost']) {
    $set   = m('common')->getSysset(array(
        'shop',
        'pay'
    ));
    if(empty($xshordersn)){
    	$order = pdo_fetch("select * from " . tablename('ewei_shop_order') . ' where id=:id and uniacid=:uniacid and openid=:openid limit 1', array(
        	':id' => $orderid,
        	':uniacid' => $uniacid,
        	':openid' => $openid
    	));
    }
    else{
    	$order = pdo_fetchall("select * from " . tablename('ewei_shop_order') . ' where xshordersn=:xshordersn and uniacid=:uniacid and openid=:openid ', array(
    			':xshordersn' => $xshordersn,
    			':uniacid' => $uniacid,
    			':openid' => $openid
    	));
    }
    if (empty($order)) {
        show_json(0, '订单未找到!');
    }
    $type = $_GPC['type'];
    if (!in_array($type, array(
        'weixin',
        'alipay',
        'unionpay'
    ))) {
        show_json(0, '未找到支付方式');
    }
    if(empty($xshordersn)){
    	$log = pdo_fetch('SELECT * FROM ' . tablename('core_paylog') . ' WHERE `uniacid`=:uniacid AND `module`=:module AND `tid`=:tid limit 1', array(
        ':uniacid' => $uniacid,
        ':module' => 'ewei_shop',
        ':tid' => $order['ordersn']
    	));
    	if (empty($log)) {
    	show_json(0, '支付出错,请重试!');
    	}
    	$order_goods = pdo_fetchall('select g.parentid,og.id,g.title, og.goodsid,og.optionid,g.total as stock,og.total as buycount,g.status,g.deleted,g.maxbuy,g.usermaxbuy,g.istime,g.timestart,g.timeend,g.buylevels,g.buygroups from  ' . tablename('ewei_shop_order_goods') . ' og ' . ' left join ' . tablename('ewei_shop_goods') . ' g on og.goodsid = g.id ' . ' where og.orderid=:orderid and og.uniacid=:uniacid ', array(
    			':uniacid' => $_W['uniacid'],
    			':orderid' => $orderid
    	));
    }
    else
    {
    	$xprice=0;
    	foreach($order as $row=>$xorder){
    		$log[] = pdo_fetch('SELECT * FROM ' . tablename('core_paylog') . ' WHERE `uniacid`=:uniacid AND `module`=:module AND `tid`=:tid limit 1', array(
    			':uniacid' => $uniacid,
    			':module' => 'ewei_shop',
    			':tid' => $xorder['ordersn']
    		));
    		$order_goods[] = pdo_fetch('select g.parentid,og.id,g.title, og.goodsid,og.optionid,g.total as stock,og.total as buycount,g.status,g.deleted,g.maxbuy,g.usermaxbuy,g.istime,g.timestart,g.timeend,g.buylevels,g.buygroups from  ' . tablename('ewei_shop_order_goods') . ' og ' . ' left join ' . tablename('ewei_shop_goods') . ' g on og.goodsid = g.id ' . ' where og.orderid=:orderid and og.uniacid=:uniacid ', array(
    				':uniacid' => $_W['uniacid'],
    				':orderid' => $xorder['id']
    		));
    		$xprice=floatval($xorder['price']+$xprice);
    	}
    	if (count($log)!=count($order)) {
    		show_json(0, '支付出错,请重试!');
    	}
    }
    foreach ($order_goods as $data) {
		if(!empty($data['parentid'])){
                $sql  = 'SELECT total FROM ' . tablename('ewei_shop_goods') . ' where id=:id limit 1';
                 $ptotal = pdo_fetchcolumn($sql, array(':id' => $data['parentid']));
            }
        if (empty($data['status']) || !empty($data['deleted'])) {
            show_json(-1, $data['title'] . '<br/> 已下架!');
        }
        if ($data['maxbuy'] > 0) {
            if ($data['buycount'] > $data['maxbuy']) {
                show_json(-1, $data['title'] . '<br/> 一次限购 ' . $data['maxbuy'] . $unit . "!");
            }
        }
        if ($data['usermaxbuy'] > 0) {
            $order_goodscount = pdo_fetchcolumn('select ifnull(sum(og.total),0)  from ' . tablename('ewei_shop_order_goods') . ' og ' . ' left join ' . tablename('ewei_shop_order') . ' o on og.orderid=o.id ' . ' where og.goodsid=:goodsid and  o.status>=1 and o.openid=:openid  and og.uniacid=:uniacid ', array(
                ':goodsid' => $data['goodsid'],
                ':uniacid' => $uniacid,
                ':openid' => $openid
            ));
            if ($order_goodscount >= $data['usermaxbuy']) {
                show_json(-1, $data['title'] . '<br/> 最多限购 ' . $data['usermaxbuy'] . $unit . "!");
            }
        }
        if ($data['istime'] == 1) {
            if (time() < $data['timestart']) {
                show_json(-1, $data['title'] . '<br/> 限购时间未到!');
            }
            if (time() > $data['timeend']) {
                show_json(-1, $data['title'] . '<br/> 限购时间已过!');
            }
        }
        if ($data['buylevels'] != '') {
            $buylevels = explode(',', $data['buylevels']);
            if (!in_array($member['level'], $buylevels)) {
                show_json(-1, '您的会员等级无法购买<br/>' . $data['title'] . '!');
            }
        }
        if ($data['buygroups'] != '') {
            $buygroups = explode(',', $data['buygroups']);
            if (!in_array($member['groupid'], $buygroups)) {
                show_json(-1, '您所在会员组无法购买<br/>' . $data['title'] . '!');
            }
        }
        if (!empty($data['optionid'])) {
            $option = pdo_fetch('select parentid,id,title,marketprice,goodssn,productsn,stock,virtual from ' . tablename('ewei_shop_goods_option') . ' where id=:id and goodsid=:goodsid and uniacid=:uniacid  limit 1', array(
                ':uniacid' => $uniacid,
                ':goodsid' => $data['goodsid'],
                ':id' => $data['optionid']
            ));
			 if(!empty($option['parentid'])){
                        $pstock = pdo_fetchcolumn('select stock from ' . tablename('ewei_shop_goods_option') . ' where id=:parentid limit 1', array(
                             ':parentid' => $option['parentid']
                        ));
                        if ( $pstock != -1) {
                             if (empty($pstock)||$order_goodscount>$pstock) {
                             show_json(-1, $data['title'] . "<br/>" . $option['title'] . " 库存不足!");
                            }
                        }
                    }
           /* if (!empty($option)) {
                if ($option['stock'] != -1) {
                    if (empty($option['stock'])) {
                        show_json(-1, $data['title'] . "<br/>" . $option['title'] . " 库存不足!");
                    }
                }
            }*/
        } else {
           if(!isset($ptotal)){
            if ($data['stock'] != -1) {
                if (empty($data['stock'])) {
                    show_json(-1, $data['title'] . "<br/>库存不足!");
                }
            }
           }
           else{
			if ($ptotal != -1) {
                    if (empty($ptotal)||$order_goodscount>$ptotal) {
                        show_json(-1, $data['title'] . "<br/>库存不足!");
                    }
				}
            }
        }
    }
  //  $plid        = $log['plid'];
    if(empty($xshordersn)){
    $param_title = $set['shop']['name'] . "订单: " . $order['ordersn'];
    }
    else
    {
    	$param_title = $set['shop']['name'] . "总订单: " . $xshordersn;
    }
	
    if ($type == 'weixin') {
        if (!is_weixin()) {
            show_json(0, '非微信环境!');
        }
        if (empty($set['pay']['weixin'])) {
            show_json(0, '未开启微信支付!');
        }
        $wechat        = array(
            'success' => false
        );
        $params        = array();
        $params['tid'] = empty($xshordersn)?$log['tid']:$xshordersn;
      /*  if (!empty($order['ordersn2'])) {
            $var = sprintf("%02d", $order['ordersn2']);
            $params['tid'] .= "GJ" . $var;
        }*/
        $params['user']  = $openid;
        $params['fee']   = empty($xprice)?$order['price']:$xprice;
        $params['title'] = $param_title;
        load()->model('payment');
        $setting = uni_setting($_W['uniacid'], array(
            'payment'
        ));
        if (is_array($setting['payment'])) {
            $options           = $setting['payment']['wechat'];
            $options['appid']  = $_W['account']['key'];
            $options['secret'] = $_W['account']['secret'];
            $wechat            = m('common')->wechat_build($params, $options, 0);
            $wechat['success'] = false;
            if (!is_error($wechat)) {
                $wechat['success'] = true;
            } else {
                show_json(0, $wechat['message']);
            }
        }
        if (!$wechat['success']) {
            show_json(0, '微信支付参数错误!');
        }
        if(empty($xshordersn)){
       		$ww=array('id' => $order['id']);
        }  
        else
        {
        	$ww=array('xshordersn' => $xshordersn);
        }
        pdo_update('ewei_shop_order', array('paytype' => 21),$ww);
        show_json(1, array(
            'wechat' => $wechat
        ));
    } else if ($type == 'alipay') {
    	if(empty($xshordersn)){
    		$ww=array('id' => $order['id']);
    	}
    	else
    	{
    		$ww=array('xshordersn' => $xshordersn);
    	}
        pdo_update('ewei_shop_order', array('paytype' => 22), $ww);
        show_json(1);
    }
} else if ($operation == 'complete' && $_W['ispost']) {
	if(empty($xshordersn)){
    	$order = pdo_fetch("select * from " . tablename('ewei_shop_order') . ' where id=:id and uniacid=:uniacid and openid=:openid limit 1', array(
       		 ':id' => $orderid,
        	':uniacid' => $uniacid,
        	':openid' => $openid
   		 ));
    	$log = pdo_fetch('SELECT * FROM ' . tablename('core_paylog') . ' WHERE `uniacid`=:uniacid AND `module`=:module AND `tid`=:tid limit 1', array(
    			':uniacid' => $uniacid,
    			':module' => 'ewei_shop',
    			':tid' => $order['ordersn']
    	));
		if (empty($order)) {
        show_json(0, '订单未找到!');
		}
		if($order['status']>0) {
        show_json(0, '订单已经支付!');
    }
	}
	else{
		$order = pdo_fetchall("select * from " . tablename('ewei_shop_order') . ' where xshordersn=:xshordersn and uniacid=:uniacid and openid=:openid ', array(
				':xshordersn' => $xshordersn,
				':uniacid' => $uniacid,
				':openid' => $openid
		));
		foreach($order as $row=>$xorder){
			$log[] = pdo_fetch('SELECT * FROM ' . tablename('core_paylog') . ' WHERE `uniacid`=:uniacid AND `module`=:module AND `tid`=:tid limit 1', array(
					':uniacid' => $uniacid,
					':module' => 'ewei_shop',
					':tid' => $xorder['ordersn']
			));
			$xprice=floatval($xorder['price']+$xprice);
			$xshsn[]=$xorder['ordersn'];
			if (empty($xorder)) {
			show_json(0, '订单未找到!');
			}
			if($xorder['status']>0) {
			show_json(0, '订单已经支付!');
			}
		}
	}
   
    $type = $_GPC['type'];
    if (!in_array($type, array(
        'weixin',
        'alipay',
        'credit',
        'cash'
    ))) {
        show_json(0, '未找到支付方式');
    }
    if (empty($log)) {
        show_json(0, '支付出错,请重试1!');
    }
   // $plid = $log['plid'];
    if ($type == 'cash') {
    	if(empty($xshordersn)){
    		$ww=array('id' => $order['id']);
    	}
    	else
    	{
    		$ww=array('xshordersn' => $xshordersn);
    	}
        pdo_update('ewei_shop_order', array('paytype' => 3), $ww);
        $ret            = array();
        $ret['result']  = 'success';
        $ret['type']    = 'cash';
        $ret['from']    = 'return';
        $ret['tid']     = empty($xshordersn)?$log['tid']:$xshordersn;
        $ret['user']    = $openid;
        $ret['fee']     = empty($xshordersn)?$log['fee']:$xprice;
        $ret['weid']    = $_W['uniacid'];
        $ret['uniacid'] = $_W['uniacid'];
        $ret['xshordersn'] = empty($xshordersn)?"":$xshordersn;
        $payresult      = $this->payResult($ret);
        if(empty($xshordersn)){
			if(strpos($order['ordersn'],'DD')!==false)
			put_status($order['ordersn'],$ret['uniacid'],"ok");
			$psn= pdo_fetch("select productsn,goodsid,total from " . tablename('ewei_shop_order_goods') . ' where orderid=:orderid limit 1', array(
			    ':orderid' => $order['id']
			));
			if(strpos($psn['productsn'],':')!==false)
			{
			     m('common')->chargell($order);
			}
			$shgoods= pdo_fetch("select parentid,costprice from " . tablename('ewei_shop_goods') . ' where id=:id limit 1', array(
			    ':id' => $psn['goodsid']
			));
			$ptid=$shgoods['parentid'];
			$acc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$order['uniacid']}'");
			$money=$acc['money'];
			$pacc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$order['puniacid']}'");
			$pmoney=$pacc['money'];
			$cosprice=floatval($shgoods['costprice']*$psn['total']);
			if(floatval($money)>=floatval($cosprice))
			{
			     if(!empty($ptid))
			     {
			         $orderflow['supuniacid']=$order['puniacid'];
			         $orderflow['saluniacid']=$order['uniacid'];
			         $orderflow['ordersn']=$order['ordersn'];
			         $orderflow['price']=$cosprice;
			         $orderflow['remark']="购买了商家：（".$pacc['name']."）的商品".$psn['total']."个，商家商品ID：".$ptid."我的商品ID：".$psn['goodsid'];
			         $orderflow['addtime']=time();
					 $nowmy=floatval($money-$cosprice);
					 $orderflow['remain']=$nowmy;
			         pdo_insert('ewei_shop_orderflow', $orderflow);
			         pdo_update('ewei_shop_order',array('pstatus'=>1),array('id'=>$order['id']));
			         $pnowmy=floatval($pmoney+$cosprice);
			         pdo_update('uni_account',array('money'=>$nowmy),array('uniacid'=>$order['uniacid']));    //下单就扣款了
			         pdo_update('uni_account',array('money'=>$pnowmy),array('uniacid'=>$order['puniacid']));    
			     }
			}
			m('common')->stock($order['id']);
        }
        else
        {
        	foreach($order as $row=>$xorder){
        		if(strpos($xorder['ordersn'],'DD')!==false)
        		put_status($xorder['ordersn'],$ret['uniacid'],"ok");
        		$psn= pdo_fetch("select productsn,goodsid,total  from " . tablename('ewei_shop_order_goods') . ' where orderid=:orderid limit 1', array(
        		    ':orderid' => $xorder['id']
        		));
        		if(strpos($psn['productsn'],':')!==false){m('common')->chargell($xorder);}
        		$shgoods= pdo_fetch("select parentid,costprice from " . tablename('ewei_shop_goods') . ' where id=:id limit 1', array(
        		    ':id' => $psn['goodsid']
        		));
        		$ptid=$shgoods['parentid'];
        	   $acc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$xorder['uniacid']}'");
			   $money=$acc['money'];
			   $pacc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$xorder['puniacid']}'");
			   $pmoney=$pacc['money'];
			   $cosprice=floatval($shgoods['costprice']*$psn['total']);
			   if(floatval($money)>=floatval($cosprice))
			   {
			     if(!empty($ptid))
			     {
			         $orderflow['supuniacid']=$xorder['puniacid'];
			         $orderflow['saluniacid']=$xorder['uniacid'];
			         $orderflow['ordersn']=$xorder['ordersn'];
			         $orderflow['price']= $cosprice;
			         $orderflow['remark']="购买了商家：（".$pacc['name']."）的商品".$psn['total']."个，商家商品ID：".$ptid."我的商品ID：".$psn['goodsid'];
			         $orderflow['addtime']=time();
					 $nowmy=floatval($money-$cosprice);
					 $orderflow['remain']=$nowmy;
			         pdo_insert('ewei_shop_orderflow', $orderflow);
			         pdo_update('ewei_shop_order',array('pstatus'=>1),array('id'=>$xorder['id']));
			         $pnowmy=floatval($pmoney+$cosprice);
			         pdo_update('uni_account',array('money'=>$nowmy),array('uniacid'=>$xorder['uniacid']));
			         pdo_update('uni_account',array('money'=>$pnowmy),array('uniacid'=>$xorder['puniacid']));    
			     }
			  }
			  m('common')->stock($xorder['id']);
        	}
        }
        show_json(2, $payresult);
    }
    $ps          = array();
    $ps['tid']   = empty($xshordersn)?$log['tid']:$xshordersn;
    $ps['user']  = $openid;
    $ps['fee']   = empty($xshordersn)?$log['fee']:$xprice;
  //  $ps['title'] = $log['title'];
    if ($type == 'credit') {
        $credits = m('member')->getCredit($openid, 'credit2');
        if ($credits < $ps['fee']) {
            show_json(0, "余额不足,请联系管理员充值");
        }
        $fee    = floatval($ps['fee']);
		if ($fee<0) {
            show_json(0, "余额不能是负数！");
        }
		else{
        $result = m('member')->setCredit($openid, 'credit2', -$fee, array(
            $_W['member']['uid'],
            '消费' . $setting['creditbehaviors']['currency'] . ':' . $fee
        ));
		}
        if (is_error($result)) {
            show_json(0, $result['message']);
        }
        $record           = array();
        $record['status'] = '1';
        $record['type']   = 'credit';
        if(empty($xshordersn)){
        	$ww=array('id' => $order['id']);
        }
        else
        {
        	$ww=array('xshordersn' => $xshordersn);
        }
        if(empty($xshsn)){
        	pdo_update('core_paylog', $record, array(
            	'plid' => $log['plid']
        	));
        }
        else
        {
        	foreach($xshsn as $row)
        	{
        		pdo_update('core_paylog', $record, array(
        			'tid' =>$row
        		));
        	}
        }
        pdo_update('ewei_shop_order', array(
            'paytype' => 1
        ), $ww);
        $ret            = array();
        $ret['result']  = 'success';
        $ret['type']    = 'credit';
        $ret['from']    = 'return';
        $ret['tid']     = empty($xshordersn)?$log['tid']:$xshordersn;
        $ret['user']    = $openid;
        $ret['fee']     = empty($xshordersn)?$log['fee']:$xprice;
        $ret['weid']    = $_W['uniacid'];
        $ret['uniacid'] = $_W['uniacid'];
        $ret['xshordersn'] = empty($xshordersn)?"":$xshordersn;
        $pay_result     = $this->payResult($ret);
        /*if(empty($xshordersn)){
            if(strpos($order['ordersn'],'DD')!==false)
                put_status($order['ordersn'],$ret['uniacid'],"ok");
            $psn= pdo_fetch("select productsn,goodsid,total from " . tablename('ewei_shop_order_goods') . ' where orderid=:orderid limit 1', array(
                ':orderid' => $order['id']
            ));
            if(strpos($psn['productsn'],':')!==false)
            {
                m('common')->chargell($order);
            }
            $shgoods= pdo_fetch("select parentid,costprice from " . tablename('ewei_shop_goods') . ' where id=:id limit 1', array(
                ':id' => $psn['goodsid']
            ));
            $ptid=$shgoods['parentid'];
            $acc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$order['uniacid']}'");
            $money=$acc['money'];
            $pacc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$order['puniacid']}'");
            $pmoney=$pacc['money'];
            $cosprice=floatval($shgoods['costprice']*$psn['total']);
            if(floatval($money)>=floatval($cosprice))
            {
                if(!empty($ptid))
                {
                    $orderflow['supuniacid']=$order['puniacid'];
                    $orderflow['saluniacid']=$order['uniacid'];
                    $orderflow['ordersn']=$order['ordersn'];
                    $orderflow['price']=$cosprice;
                    $orderflow['remark']="购买了商家：（".$pacc['name']."）的商品".$psn['total']."个，商家商品ID：".$ptid."我的商品ID：".$psn['goodsid'];
                    $orderflow['addtime']=time();
					$nowmy=floatval($money-$cosprice);
					$orderflow['remain']=$nowmy;
                    pdo_insert('ewei_shop_orderflow', $orderflow);
                    pdo_update('ewei_shop_order',array('pstatus'=>1),array('id'=>$order['id']));
                    $pnowmy=floatval($pmoney+$cosprice);
                    pdo_update('uni_account',array('money'=>$nowmy),array('uniacid'=>$order['uniacid']));    //下单就扣款了
                    pdo_update('uni_account',array('money'=>$pnowmy),array('uniacid'=>$order['puniacid']));
                }
            }
			m('common')->stock($order['id']);
        }
        else
        {
            foreach($order as $row=>$xorder){
                if(strpos($xorder['ordersn'],'DD')!==false)
                    put_status($xorder['ordersn'],$ret['uniacid'],"ok");
                $psn= pdo_fetch("select productsn,goodsid,total  from " . tablename('ewei_shop_order_goods') . ' where orderid=:orderid limit 1', array(
                    ':orderid' => $xorder['id']
                ));
                if(strpos($psn['productsn'],':')!==false){m('common')->chargell($xorder);}
                $shgoods= pdo_fetch("select parentid,costprice from " . tablename('ewei_shop_goods') . ' where id=:id limit 1', array(
                    ':id' => $psn['goodsid']
                ));
                $ptid=$shgoods['parentid'];
                $acc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$xorder['uniacid']}'");
                $money=$acc['money'];
                $pacc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$xorder['puniacid']}'");
                $pmoney=$pacc['money'];
                $cosprice=floatval($shgoods['costprice']*$psn['total']);
                if(floatval($money)>=floatval($cosprice))
                {
                    if(!empty($ptid))
                    {
                        $orderflow['supuniacid']=$xorder['puniacid'];
                        $orderflow['saluniacid']=$xorder['uniacid'];
                        $orderflow['ordersn']=$xorder['ordersn'];
                        $orderflow['price']= $cosprice;
                        $orderflow['remark']="购买了商家：（".$pacc['name']."）的商品".$psn['total']."个，商家商品ID：".$ptid."我的商品ID：".$psn['goodsid'];
                        $orderflow['addtime']=time();
						$nowmy=floatval($money-$cosprice);
						$orderflow['remain']=$nowmy;
                        pdo_insert('ewei_shop_orderflow', $orderflow);
                        pdo_update('ewei_shop_order',array('pstatus'=>1),array('id'=>$xorder['id']));
                        $pnowmy=floatval($pmoney+$cosprice);
                        pdo_update('uni_account',array('money'=>$nowmy),array('uniacid'=>$xorder['uniacid']));
                        pdo_update('uni_account',array('money'=>$pnowmy),array('uniacid'=>$xorder['puniacid']));
                    }
                }
				m('common')->stock($xorder['id']);
            }
        }*/
        show_json(1, $pay_result);
    } else if ($type == 'weixin') {
    	if(empty($xshordersn)){
       		 $ordersn = $order['ordersn'];
    	}
    	else
    	{
    		$ordersn=$xshordersn;
    	}
      /*  if (!empty($order['ordersn2'])) {
            $ordersn .= "GJ" . sprintf("%02d", $order['ordersn2']);
        }*/
        $payquery = m('finance')->isWeixinPay($ordersn);     //查询订单
        if (!is_error($payquery)) {
            $record           = array();
            $record['status'] = '1';
            $record['type']   = 'wechat';
            if(empty($xshordersn)){
           	 	pdo_update('core_paylog', $record, array('plid' => $log['plid']));
            }
            else 
            {
            	foreach($xshsn as $row1)
            	{
            		pdo_update('core_paylog', $record, array('tid' =>$row1));
            	}
            }
            $ret            = array();
            $ret['result']  = 'success';
            $ret['type']    = 'wechat';
            $ret['from']    = 'return';
            $ret['tid']     = empty($xshordersn)?$log['tid']:$xshordersn;
            $ret['user']    = $openid;
            $ret['fee']     = empty($xshordersn)?$log['fee']:$xprice;
            $ret['weid']    = $_W['uniacid'];
            $ret['uniacid'] = $_W['uniacid'];
            $ret['xshordersn'] = empty($xshordersn)?"":$xshordersn;
            $ret['deduct']  = intval($_GPC['deduct']) == 1;
            $pay_result     = $this->payResult($ret);
           /* if(empty($xshordersn)){
                    if(strpos($order['ordersn'],'DD')!==false)
                        put_status($order['ordersn'],$ret['uniacid'],"ok");
                    $psn= pdo_fetch("select productsn,goodsid,total from " . tablename('ewei_shop_order_goods') . ' where orderid=:orderid limit 1', array(
                        ':orderid' => $order['id']
                    ));
                    if(strpos($psn['productsn'],':')!==false)
                    {
                        m('common')->chargell($order);
                    }
                    $shgoods= pdo_fetch("select parentid,costprice from " . tablename('ewei_shop_goods') . ' where id=:id limit 1', array(
                        ':id' => $psn['goodsid']
                    ));
                    $ptid=$shgoods['parentid'];
                    $acc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$order['uniacid']}'");
                    $money=$acc['money'];
                    $pacc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$order['puniacid']}'");
                    $pmoney=$pacc['money'];
                    $cosprice=floatval($shgoods['costprice']*$psn['total']);
                    if(floatval($money)>=floatval($cosprice))
                    {
                        if(!empty($ptid))
                        {
                            $orderflow['supuniacid']=$order['puniacid'];
                            $orderflow['saluniacid']=$order['uniacid'];
                            $orderflow['ordersn']=$order['ordersn'];
                            $orderflow['price']=$cosprice;
                            $orderflow['remark']="购买了商家：（".$pacc['name']."）的商品".$psn['total']."个，商家商品ID：".$ptid."我的商品ID：".$psn['goodsid'];
                            $orderflow['addtime']=time();
							$nowmy=floatval($money-$cosprice);
							$orderflow['remain']=$nowmy;
                            pdo_insert('ewei_shop_orderflow', $orderflow);
                            pdo_update('ewei_shop_order',array('pstatus'=>1),array('id'=>$order['id']));
                         
                            $pnowmy=floatval($pmoney+$cosprice);
                            pdo_update('uni_account',array('money'=>$nowmy),array('uniacid'=>$order['uniacid']));    //下单就扣款了
                            pdo_update('uni_account',array('money'=>$pnowmy),array('uniacid'=>$order['puniacid']));
                        }
                    }
					m('common')->stock($order['id']);
                }
                else
                {
                    foreach($order as $row=>$xorder){
                        if(strpos($xorder['ordersn'],'DD')!==false)
                            put_status($xorder['ordersn'],$ret['uniacid'],"ok");
                        $psn= pdo_fetch("select productsn,goodsid,total  from " . tablename('ewei_shop_order_goods') . ' where orderid=:orderid limit 1', array(
                            ':orderid' => $xorder['id']
                        ));
                        if(strpos($psn['productsn'],':')!==false){m('common')->chargell($xorder);}
                        $shgoods= pdo_fetch("select parentid,costprice from " . tablename('ewei_shop_goods') . ' where id=:id limit 1', array(
                            ':id' => $psn['goodsid']
                        ));
                        $ptid=$shgoods['parentid'];
                        $acc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$xorder['uniacid']}'");
                        $money=$acc['money'];
                        $pacc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$xorder['puniacid']}'");
                        $pmoney=$pacc['money'];
                        $cosprice=floatval($shgoods['costprice']*$psn['total']);
                        if(floatval($money)>=floatval($cosprice))
                        {
                            if(!empty($ptid))
                            {
                                $orderflow['supuniacid']=$xorder['puniacid'];
                                $orderflow['saluniacid']=$xorder['uniacid'];
                                $orderflow['ordersn']=$xorder['ordersn'];
                                $orderflow['price']= $cosprice;
                                $orderflow['remark']="购买了商家：（".$pacc['name']."）的商品".$psn['total']."个，商家商品ID：".$ptid."我的商品ID：".$psn['goodsid'];
                                $orderflow['addtime']=time();
								$nowmy=floatval($money-$cosprice);
								$orderflow['remain']=$nowmy;
                                pdo_insert('ewei_shop_orderflow', $orderflow);
                                pdo_update('ewei_shop_order',array('pstatus'=>1),array('id'=>$xorder['id']));
                             
                                $pnowmy=floatval($pmoney+$cosprice);
                                pdo_update('uni_account',array('money'=>$nowmy),array('uniacid'=>$xorder['uniacid']));
                                pdo_update('uni_account',array('money'=>$pnowmy),array('uniacid'=>$xorder['puniacid']));
                            }
                        }
						m('common')->stock($xorder['id']);
                    }
                }*/
        show_json(1, $pay_result);
       }
    show_json(0, '支付出错,请重试!');
    }
} else if ($operation == 'return') {
	$xsh="";
    $tid = $_GPC['out_trade_no'];
    if (!m('finance')->isAlipayNotify($_GET)) {
        die('支付出现错误，请重试!');
    }
    if(strpos($tid,'XSH')==false){
    	$log = pdo_fetch('SELECT * FROM ' . tablename('core_paylog') . ' WHERE `uniacid`=:uniacid AND `module`=:module AND `tid`=:tid limit 1', array(
        	':uniacid' => $uniacid,
        	':module' => 'ewei_shop',
        	':tid' => $tid
    	));
    	$order = pdo_fetchall("select * from " . tablename('ewei_shop_order') . ' where ordersn=:ordersn ', array(
    	    ':ordersn' => $tid
    	));
    }
    else
    {
    	$xsh="xsh";
    	$order = pdo_fetchall("select * from " . tablename('ewei_shop_order') . ' where xshordersn=:xshordersn ', array(
				':xshordersn' => $tid
		));
    	$xprice=0;
		foreach($order as $row=>$xorder){
			$ll[] = pdo_fetch('SELECT * FROM ' . tablename('core_paylog') . ' WHERE `uniacid`=:uniacid AND `module`=:module AND `tid`=:tid limit 1', array(
					':uniacid' => $uniacid,
					':module' => 'ewei_shop',
					':tid' => $xorder['ordersn']
			));
			$xprice=floatval($xorder['price']+$xprice);
		}
		$log=$ll[0];
    }
    if (empty($log)) {
        die('支付出现错误，请重试!');
    }
    if ($log['status'] != 1) {
        $record           = array();
        $record['status'] = '1';
        $record['type']   = 'alipay';
        if(empty($xsh)){
       	 	pdo_update('core_paylog', $record, array(
            'plid' => $log['plid']
        	));
        }
        else
        {
        	foreach($ll as $rr=>$rows)
        	{
        		pdo_update('core_paylog', $record, array(
        				'plid' => $rows['plid']
        		));
        	}
        }
        $ret            = array();
        $ret['result']  = 'success';
        $ret['type']    = 'alipay';
        $ret['from']    = 'return';
        $ret['tid']     = empty($xsh)?$log['tid']:$tid;
        $ret['user']    = $log['openid'];
        $ret['fee']     = empty($xsh)?$log['fee']:$xprice;
        $ret['weid']    = $log['uniacid'];
        $ret['uniacid'] = $log['uniacid'];
        $this->payResult($ret);
        if(strpos($tid,'XSH')==false){
                    if(strpos($order['ordersn'],'DD')!==false)
                        put_status($order['ordersn'],$ret['uniacid'],"ok");
                    $psn= pdo_fetch("select productsn,goodsid,total from " . tablename('ewei_shop_order_goods') . ' where orderid=:orderid limit 1', array(
                        ':orderid' => $order['id']
                    ));
                    if(strpos($psn['productsn'],':')!==false)
                    {
                        m('common')->chargell($order);
                    }
                    $shgoods= pdo_fetch("select parentid,costprice from " . tablename('ewei_shop_goods') . ' where id=:id limit 1', array(
                        ':id' => $psn['goodsid']
                    ));
                    $ptid=$shgoods['parentid'];
                    $acc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$order['uniacid']}'");
                    $money=$acc['money'];
                    $pacc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$order['puniacid']}'");
                    $pmoney=$pacc['money'];
                    $cosprice=floatval($shgoods['costprice']*$psn['total']);
                    if(floatval($money)>=floatval($cosprice))
                    {
                        if(!empty($ptid))
                        {
                            $orderflow['supuniacid']=$order['puniacid'];
                            $orderflow['saluniacid']=$order['uniacid'];
                            $orderflow['ordersn']=$order['ordersn'];
                            $orderflow['price']=$cosprice;
                            $orderflow['remark']="购买了商家：（".$pacc['name']."）的商品".$psn['total']."个，商家商品ID：".$ptid."我的商品ID：".$psn['goodsid'];
                            $orderflow['addtime']=time();
							$nowmy=floatval($money-$cosprice);
							$orderflow['remain']=$nowmy;
                            pdo_insert('ewei_shop_orderflow', $orderflow);
                            pdo_update('ewei_shop_order',array('pstatus'=>1),array('id'=>$order['id']));
                         
                            $pnowmy=floatval($pmoney+$cosprice);
                            pdo_update('uni_account',array('money'=>$nowmy),array('uniacid'=>$order['uniacid']));    //下单就扣款了
                            pdo_update('uni_account',array('money'=>$pnowmy),array('uniacid'=>$order['puniacid']));
                        }
                    }
					m('common')->stock($order['id']);
                }
                else
                {
                    foreach($order as $row=>$xorder){
                        if(strpos($xorder['ordersn'],'DD')!==false)
                            put_status($xorder['ordersn'],$ret['uniacid'],"ok");
                        $psn= pdo_fetch("select productsn,goodsid,total  from " . tablename('ewei_shop_order_goods') . ' where orderid=:orderid limit 1', array(
                            ':orderid' => $xorder['id']
                        ));
                        if(strpos($psn['productsn'],':')!==false){m('common')->chargell($xorder);}
                        $shgoods= pdo_fetch("select parentid,costprice from " . tablename('ewei_shop_goods') . ' where id=:id limit 1', array(
                            ':id' => $psn['goodsid']
                        ));
                        $ptid=$shgoods['parentid'];
                        $acc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$xorder['uniacid']}'");
                        $money=$acc['money'];
                        $pacc = pdo_fetch("SELECT name,money FROM " . tablename('uni_account') . " WHERE `uniacid` = '{$xorder['puniacid']}'");
                        $pmoney=$pacc['money'];
                        $cosprice=floatval($shgoods['costprice']*$psn['total']);
                        if(floatval($money)>=floatval($cosprice))
                        {
                            if(!empty($ptid))
                            {
                                $orderflow['supuniacid']=$xorder['puniacid'];
                                $orderflow['saluniacid']=$xorder['uniacid'];
                                $orderflow['ordersn']=$xorder['ordersn'];
                                $orderflow['price']= $cosprice;
                                $orderflow['remark']="购买了商家：（".$pacc['name']."）的商品".$psn['total']."个，商家商品ID：".$ptid."我的商品ID：".$psn['goodsid'];
                                $orderflow['addtime']=time();
								$nowmy=floatval($money-$cosprice);
								$orderflow['remain']=$nowmy;
                                pdo_insert('ewei_shop_orderflow', $orderflow);
                                pdo_update('ewei_shop_order',array('pstatus'=>1),array('id'=>$xorder['id']));
                             
                                $pnowmy=floatval($pmoney+$cosprice);
                                pdo_update('uni_account',array('money'=>$nowmy),array('uniacid'=>$xorder['uniacid']));
                                pdo_update('uni_account',array('money'=>$pnowmy),array('uniacid'=>$xorder['puniacid']));
                            }
                        }
						m('common')->stock($xorder['id']);
                    }
                }
            
    }
    if(empty($xsh)){
    	$orderid = pdo_fetchcolumn('select id from ' . tablename('ewei_shop_order') . ' where ordersn=:ordersn and uniacid=:uniacid', array(
       	 	':ordersn' => $log['tid'],
        	':uniacid' => $_W['uniacid']
    	));
    	$url     = $this->createMobileUrl('order/detail', array(
        	'id' => $orderid
   		 ));
    }
    else
    {
    	$url= $this->createMobileUrl('order/list');
    }
    die("<script>top.window.location.href='{$url}'</script>");
}
if ($operation == 'display') {
    include $this->template('order/pay');
}