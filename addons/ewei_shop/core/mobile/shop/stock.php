<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$openid    = m('user')->getOpenid();
if ($_W['isajax']) {
if ($operation == 'display') {
    $pindex    = max(1, intval($_GPC['page']));
    $psize     = 10;
	$cauth    = pdo_fetchall("SELECT goodsid,title FROM " . tablename('ewei_shop_authgoods') . " WHERE uniacid=:uniacid  and openid=:openid ", array(
            ':uniacid' => $_W['uniacid'],
            ':openid' =>$openid
        ));
	$catename=array();
	if(count($cauth)>0){
		foreach($cauth as $ca=>$cc)
		{
			$catename[$cc['goodsid']]=$cc['title'];
		}
	}
	
	
    $condition = " and t1.uniacid=:uniacid and t1.openid=:openid and t1.status=1  and t1.refundid=0 ";
	$goodcon='';
	if(!empty($_GPC['keyword']))
	{
		$goodcon = " and title like '%".$_GPC['keyword']."%' ";
	}	
    $params    = array(
        ':uniacid' => $_W['uniacid'],
        ':openid' => $openid
    );
	$sql="SELECT t2.goodsid,sum(t2.total) as ostock FROM " . tablename('ewei_shop_order') ." as t1 left join ".tablename('ewei_shop_order_goods')." as t2 on t1.id=t2.orderid ";
	$sql .=" where 1=1 $condition group by t2.goodsid ORDER BY t2.goodsid DESC LIMIT " . ($pindex - 1) * $psize . ',' . $psize;	
    $list  = pdo_fetchall($sql , $params);
    foreach($list as $ll=>$l)
	{
		
		$goods= pdo_fetch(" SELECT id,title,thumb,marketprice,productprice,ccate FROM " . tablename('ewei_shop_goods') ." where 1=1  and id=".$l['goodsid'].$goodcon);
		if($goods['id']>0)
		{
			$list[$ll] = array_merge($list[$ll],$goods); 
			$list[$ll]['ctitle']=$catename[$goods['ccate']];
		}
		else
		{
			unset($list[$ll]);
		}
	}
	if(!(count($list)>0)){
		show_json(1, array(
			 'total'=>0
			));
	}
	show_json(1, array(
        'list' => $list
    ));
}
} 
include $this->template('shop/stock');