<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$openid    = m('user')->getOpenid();
$uniacid   = $_W['uniacid'];
$ccate=empty($_GPC['ccate'])?0:$_GPC['ccate'];
$level     = m('member')->getLevel($openid);
$downprice=empty($_GPC['downprice'])?0:$_GPC['downprice'];
$levels =m('member')->getLevels();
$disgoods=array();
if ($_W['isajax']) {
    if ($operation == 'display') {
        $pindex    = max(1, intval($_GPC['page']));
        $psize     = 10;
		$condition = ' and `uniacid` = :uniacid AND `deleted` = 0 and status=1';
		$goodsstr='';
		$list=array();
        $cauth    = pdo_fetchall("SELECT goodsid,title FROM " . tablename('ewei_shop_authgoods') . " WHERE uniacid=:uniacid  and openid=:openid  and status=1 and starttime<:time and endtime>:time", array(
            ':uniacid' => $_W['uniacid'],
            ':openid' =>$openid,
            ':time'   =>time()
        ));
		$authcate=array();
		$catename=array();
		
		if(count($cauth)>0){
			foreach($cauth as $ca=>$cc)
			{
				$authcate[]=$cc['goodsid'];
				$catename[$cc['goodsid']]=$cc['title'];
			}
			$goodsstr="and ccate in ( ". implode(",",$authcate) ." )";
		}
		else
		{
			 show_json(1, array(
			    'total'=>0
        ));
		}
		if(!empty($_GPC['ccate']))
		{
			$goodsstr=" and ccate=".$_GPC['ccate'];
		}
		$list = pdo_fetchall("SELECT id,title,thumb,marketprice,productprice,ccate,discounts,isnodiscount FROM " . tablename('ewei_shop_goods') . "  WHERE 1=1 $condition $goodsstr  ORDER BY `id` DESC LIMIT " . ($pindex - 1) * $psize . ',' . $psize, 
		array(
			 ':uniacid' => $_W['uniacid']
		));
		 
		$list = set_medias($list, 'thumb');
		foreach($list as &$ll)
		{
			$ll['ctitle']=$catename[$ll['ccate']];
			$discounts = json_decode($ll['discounts'], true);
            if (is_array($discounts)) {
                if (!empty($level['id'])) {
                    if ($discounts['level' . $level['id']] > 0 && $discounts['level' . $level['id']] < 10) {
                        $ll['discount'] = $discounts['level' . $level['id']];
                    }
                } else {
                    if ($discounts['default'] > 0 && $discounts['default'] < 10) {
                        $ll['discount'] = $discounts['default'];
                    }
                }
          
				if($ll['discount']>0&&$ll['discount']<10&&empty($ll['isnodiscount']))
				{
					$ll['price']=round($ll['marketprice']*$ll['discount']/10,2);
				}
				else
				{
					$ll['price']=$ll['marketprice'];
				}
            }
		}
        show_json(1, array(
			'ccate'=>$_GPC['ccate'],
			'cauth'=> $cauth,
            'list' => $list,
            'pagesize' => $psize
        ));
    }
}
include $this->template('shop/price');