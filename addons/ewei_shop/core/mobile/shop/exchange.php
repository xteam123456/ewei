<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$openid    = m('user')->getOpenid();
$uniacid   = $_W['uniacid'];
if ($operation == 'display') {
        $cpwd=$_GPC['pwd'];
		if(!empty($cpwd))
		{
		    $sql="SELECT tb1.status,tb2.endtime,tb2.goodsid,tb2.cdmoney,tb1.id FROM " . tablename('ewei_shop_newcard') . " as tb1 left join ". tablename('ewei_shop_typecard')." as tb2 on tb1.typeid=tb2.id ";
            $sql .="  WHERE tb1.uniacid = :uniacid and tb1.cpwd=:cpwd ";
		    $data = pdo_fetch($sql, array(
		        ':uniacid' => $uniacid,
		        ':cpwd' =>$cpwd
		    ));
		    if(empty($data))
		    {
		        show_json(2, array("message"=>"不存在该卡券！"));
		    }
		    else{
		      $status=$data['status'];
		      if($status==1)
		      {
		          show_json(2, array("message"=>"该卡券已经使用！"));
		      }
		      if($status==2)
		      {
		          show_json(2, array("message"=>"该卡券已经作废！"));
		      }
		      if(time()>$data['endtime'])
		      {
		           show_json(2, array("message"=>"该卡券已经过期！"));
		      }
		      show_json(1, $data);
		    }
		}
		$adv = pdo_fetch("select id,thumb from " . tablename('ewei_shop_advtype') . " where uniacid=:uniacid and typename like '%卡券%'", array(
		    ":uniacid" => $uniacid
		));
		$advs     = pdo_fetchall("select id,advname,link,thumb from " . tablename('ewei_shop_adv') . ' where uniacid=:uniacid and typeid=:typeid and enabled=1 order by displayorder desc', array(
		    ':uniacid' => $uniacid,
		    ':typeid' =>$adv['id']
		));
		$advs     = set_medias($advs, 'thumb');
}
include $this->template('shop/exchange');